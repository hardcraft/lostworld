package com.gmail.val59000mc.lostworld.callbacks;

import com.gmail.val59000mc.hcgameslib.api.impl.DefaultPluginCallbacks;
import com.gmail.val59000mc.hcgameslib.api.impl.HCMessage;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.stuff.Stuff;
import com.gmail.val59000mc.hcgameslib.worlds.WorldConfig;
import com.gmail.val59000mc.hcgameslib.worlds.WorldManager;
import com.gmail.val59000mc.lostworld.LostWorld;
import com.gmail.val59000mc.lostworld.common.Constants;
import com.gmail.val59000mc.lostworld.events.BobSpawnEvent;
import com.gmail.val59000mc.lostworld.events.FillChestEvent;
import com.gmail.val59000mc.lostworld.items.ChestsManager;
import com.gmail.val59000mc.lostworld.items.LostWorldtems;
import com.gmail.val59000mc.lostworld.objectives.GameObjective;
import com.gmail.val59000mc.lostworld.objectives.ObjectiveManager;
import com.gmail.val59000mc.lostworld.objectives.ObjectiveType;
import com.gmail.val59000mc.lostworld.objectives.impl.TrophiesObjective;
import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;
import com.gmail.val59000mc.lostworld.players.LostWorldTeam;
import com.gmail.val59000mc.lostworld.scoreboard.ScoreboardManager;
import com.gmail.val59000mc.spigotutils.DeleteFileVisitor;
import com.gmail.val59000mc.spigotutils.OverwriteFileVisitor;
import com.gmail.val59000mc.spigotutils.Parser;
import com.gmail.val59000mc.spigotutils.Texts;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import io.lumine.xikage.mythicmobs.MythicMobs;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class LostWorldCallbacks extends DefaultPluginCallbacks {

    private ObjectiveManager objectiveManager;
    private ScoreboardManager scoreboardManager;
    private ChestsManager chestsManager;

    public ObjectiveManager getObjectiveManager() {
        return objectiveManager;
    }

    public void setObjectiveManager(ObjectiveManager objectiveManager) {
        this.objectiveManager = objectiveManager;
    }


    public ScoreboardManager getScoreboardManager() {
        return scoreboardManager;
    }

    public void setScoreboardManager(ScoreboardManager scoreboardManager) {
        this.scoreboardManager = scoreboardManager;
    }


    public void setChestsManager(ChestsManager chestsManager) {
        this.chestsManager = chestsManager;
    }

    public ChestsManager getChestsManager() {
        return chestsManager;
    }

    @Override
    public HCPlayer newHCPlayer(Player player) {
        return new LostWorldPlayer(player);
    }

    @Override
    public HCPlayer newHCPlayer(HCPlayer hcPlayer) {
        return new LostWorldPlayer(hcPlayer);
    }

    @Override
    public List<HCTeam> createTeams() {

        World world = getApi().getWorldConfig().getWorld();
        FileConfiguration cfg = getConfig();
        ;
        Location redInitial = Parser.parseLocation(world, cfg.getString("teams.red.initial"));
        Location redCastle = Parser.parseLocation(world, cfg.getString("teams.red.castle"));
        HCTeam red = new LostWorldTeam(Constants.RED_TEAM, ChatColor.RED, redInitial, redCastle);

        Location greenInitial = Parser.parseLocation(world, cfg.getString("teams.green.initial"));
        Location greenCastle = Parser.parseLocation(world, cfg.getString("teams.green.castle"));
        HCTeam green = new LostWorldTeam(Constants.GREEN_TEAM, ChatColor.GREEN, greenInitial, greenCastle);

        return Lists.newArrayList(red, green);

    }

    @Override
    public WorldConfig configureWorld(WorldManager wm) {
        WorldConfig worldCfg = super.configureWorld(wm);
        try {
            installMythicMobsConfigFiles(wm);
        } catch (IOException e) {
            throw new IllegalStateException("Could not install Mythic Mobs config files", e);
        }
        return worldCfg;
    }

    private void installMythicMobsConfigFiles(WorldManager wm) throws IOException {

        LostWorld lostWorldPlugin = JavaPlugin.getPlugin(LostWorld.class);
        MythicMobs mythicMobsPlugin = JavaPlugin.getPlugin(MythicMobs.class);
        File pluginsFile = lostWorldPlugin.getDataFolder().getParentFile();
        File mythicMobDir = mythicMobsPlugin.getDataFolder();
        InputStream embeddedConfig = lostWorldPlugin.getResource("MythicMobs.zip");
        ZipInputStream zipInput = new ZipInputStream(embeddedConfig);
        Preconditions.checkNotNull(embeddedConfig, "missing mythic mobs config files for lostworld in classpath");


        File tmp = new File(pluginsFile, "MythicMobs.tmp");
        Path tmpPath = tmp.toPath();
        if (tmp.exists()) {
            Files.walkFileTree(tmpPath, new DeleteFileVisitor());
        }
        Files.createDirectory(tmpPath);

        ZipEntry zipEntry;
        byte[] buffer = new byte[1024];
        while ((zipEntry = zipInput.getNextEntry()) != null) {
            File newFile = new File(tmp, zipEntry.getName());
            if (zipEntry.isDirectory()) {
                newFile.mkdir();
            } else {
                try (FileOutputStream fos = new FileOutputStream(newFile)) {
                    int len;
                    while ((len = zipInput.read(buffer)) > 0) {
                        fos.write(buffer, 0, len);
                    }
                }
            }
        }

//        Files.copy(embeddedConfig, tmpPath, StandardCopyOption.REPLACE_EXISTING);

        if (mythicMobDir.exists()) {
            Files.walkFileTree(mythicMobDir.toPath(), new DeleteFileVisitor());
        }
        mythicMobDir.mkdir();
        Files.walkFileTree(tmpPath, new OverwriteFileVisitor(tmpPath, mythicMobDir.toPath()));

        Files.walkFileTree(tmpPath, new DeleteFileVisitor());

        // force reload mythic mobs
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "mm reload");
        MythicMobs.inst().getEntityManager().refreshCaches();
    }

    @Override
    public List<Stuff> createStuffs() {

        LostWorldTeam redTeam = (LostWorldTeam) getPmApi().getHCTeam(Constants.RED_TEAM);
        redTeam.setInitialStuff(
            new Stuff.Builder(Constants.INITIAL_RED_STUFF)
                .addArmorItems(LostWorldtems.getArmor(Color.RED))
                .addInventoryItems(LostWorldtems.getInitialItems())
                .build()
        );
        redTeam.setStuff(
            new Stuff.Builder(Constants.RED_STUFF)
                .addArmorItems(LostWorldtems.getArmor(Color.RED))
                .addInventoryItems(LostWorldtems.getItems())
                .build()
        );

        LostWorldTeam greenTeam = (LostWorldTeam) getPmApi().getHCTeam(Constants.GREEN_TEAM);
        greenTeam.setInitialStuff(
            new Stuff.Builder(Constants.INITIAL_GREEN_STUFF)
                .addArmorItems(LostWorldtems.getArmor(Color.GREEN))
                .addInventoryItems(LostWorldtems.getInitialItems())
                .build()
        );
        greenTeam.setStuff(
            new Stuff.Builder(Constants.GREEN_STUFF)
                .addArmorItems(LostWorldtems.getArmor(Color.GREEN))
                .addInventoryItems(LostWorldtems.getItems())
                .build()
        );

        return Lists.newArrayList(
            redTeam.getInitialStuff(),
            redTeam.getStuff(),
            greenTeam.getInitialStuff(),
            greenTeam.getStuff()
        );
    }


    @Override
    public void assignStuffToPlayer(HCPlayer hcPlayer) {

        if (hcPlayer.getStuff() == null && hcPlayer.hasTeam() && hcPlayer.isOnline()) {
            hcPlayer.setStuff(((LostWorldTeam) hcPlayer.getTeam()).getInitialStuff());
        }

    }

    @Override
    public void startPlayer(HCPlayer hcPlayer) {
        super.startPlayer(hcPlayer);
        hcPlayer.getPlayer().setGameMode(GameMode.SURVIVAL);
    }

    @Override
    public void respawnPlayer(HCPlayer hcPlayer) {
        super.respawnPlayer(hcPlayer);
        if (hcPlayer.isOnline()) {
            Block blockUnderFeet = hcPlayer.getPlayer().getLocation().add(0, -1, 0).getBlock();
            blockUnderFeet.setType(Material.STONE);
        }
    }

    @Override
    public void handleKillEvent(PlayerDeathEvent event, HCPlayer hcKilled, HCPlayer hcKiller) {
        getPmApi().autoRespawnPlayerAfter20Ticks(event.getEntity());
        List<ItemStack> drops = event.getDrops();
        LostWorldtems.filterDrops(drops);
        TrophiesObjective trophies = (TrophiesObjective) objectiveManager.getObjective(ObjectiveType.TROPHIES);
        if (trophies.isStarted() && !trophies.isCompleted() && hcKilled.hasTeam() && hcKilled.is(PlayerState.PLAYING)) {
            drops.add(LostWorldtems.getTrophyItem(5));
        }
    }


    @Override
    public void handleDeathEvent(PlayerDeathEvent event, HCPlayer hcKilled) {
        super.handleDeathEvent(event, hcKilled); // auto respawn
        LostWorldtems.filterDrops(event.getDrops());
    }

    @Override
    public void formatDeathMessage(HCPlayer hcKilled, PlayerDeathEvent event) {
        Entity lastDamagerEntity = getPmApi().getLastDamagingEntityArrowShooter(hcKilled);
        if (lastDamagerEntity != null
            && lastDamagerEntity.getType().equals(EntityType.SKELETON)
            && Constants.BOB_DISPLAY_NAME.equals(lastDamagerEntity.getCustomName())) {
            event.setDeathMessage(getStringsApi().get("lostworld.objectives.BOB.bob-kills-player")
                .replace("%player%", hcKilled.getColoredName())
                .toString());
        } else {
            super.formatDeathMessage(hcKilled, event);
        }
    }

    @Override
    public List<String> updatePlayingScoreboard(HCPlayer hcPlayer) {
        return scoreboardManager.getPlayingScoreboardContent((LostWorldPlayer) hcPlayer);
    }

    @Override
    public void remainingTimeBeforeEnd(int remainingTime, int timeElapsed) {
        super.remainingTimeBeforeEnd(remainingTime, timeElapsed);

        if (remainingTime == 0) {
            objectiveManager.forceCompleteAllUncompletedObjectives();
        }

        // drop supply chest every 10 minutes (8,18,28,38,48,58,68)
        if (timeElapsed % 480 == 0 && remainingTime > 600) {
            getApi().callEvent(new FillChestEvent(getApi()));
        }

        // drop °°°BoB°°° head after 40 minutes
        if (timeElapsed == 2400) {
            getApi().callEvent(new BobSpawnEvent(getApi()));
        }

    }

    @Override
    public HCTeam getForcedOnlineWinningTeam(List<HCTeam> onlineTeams) {
        LostWorldTeam winner = null;
        boolean isSameScore = false;
        for (HCTeam team : onlineTeams) {
            LostWorldTeam lwTeam = (LostWorldTeam) team;
            if (lwTeam.getScore() > 0) {
                if (winner == null) {
                    winner = lwTeam;
                } else {
                    if (lwTeam.getScore() == winner.getScore()) {
                        isSameScore = true;
                    } else if (lwTeam.getScore() > winner.getScore()) {
                        isSameScore = false;
                        winner = lwTeam;
                    }
                }
            }
        }
        if (isSameScore) {
            return null;
        }

        return winner;
    }

    @Override
    public void printEndMessage(HCPlayer hcPlayer, HCTeam winningTeam) {

        if (hcPlayer.isOnline()) {

            LostWorldPlayer lwPlayer = (LostWorldPlayer) hcPlayer;

            HCMessage msg = getStringsApi().get("messages.end.winning-team")
                .replace("%game%", getApi().getName())
                .replace("%kills%", String.valueOf(hcPlayer.getKills()))
                .replace("%deaths%", String.valueOf(hcPlayer.getDeaths()))
                .replace("%hardcoins%", String.valueOf(hcPlayer.getMoney()))
                .replace("%timeplayed%", String.valueOf(hcPlayer.getTimePlayed()))
                .replace("%winner%", (winningTeam == null ? "Match nul" : winningTeam.getColor() + winningTeam.getName()));

            formatEndMessageWithObjective(msg, objectiveManager.getObjective(ObjectiveType.CASTLE));
            formatEndMessageWithObjective(msg, objectiveManager.getObjective(ObjectiveType.ZOMBIES));
            formatEndMessageWithObjective(msg, objectiveManager.getObjective(ObjectiveType.KILLS));
            formatEndMessageWithObjective(msg, objectiveManager.getObjective(ObjectiveType.TROPHIES));
            formatEndMessageWithObjective(msg, objectiveManager.getObjective(ObjectiveType.BOB));
            formatEndMessageWithObjective(msg, objectiveManager.getObjective(ObjectiveType.TOWERS));

            Texts.tellraw(hcPlayer.getPlayer(), msg.toString());

        }

    }

    private void formatEndMessageWithObjective(HCMessage msg, GameObjective obj) {
        String winningTeam = obj.isCompleted()
            ? obj.getCompletedByTeam().getColoredName()
            : getStringsApi().get("lostworld.objectives.empty-completed-by-team").toString();
        msg.replace("%" + obj.getType().name() + "%", winningTeam);
    }

    /**
     * Play a sound and display a title when starting the game
     */
    @Override
    public void startGameMessages() {

        super.startGameMessages();

        for (HCTeam team : getPmApi().getTeams()) {
            getStringsApi().get("lostworld.help-bar-when-starting")
                .replace("%color%", team.getColoredName())
                .sendActionBar(team.getMembers(true, PlayerState.PLAYING), 10);
        }

    }

}
