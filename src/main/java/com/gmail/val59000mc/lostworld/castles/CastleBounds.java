package com.gmail.val59000mc.lostworld.castles;


import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import com.google.common.base.MoreObjects;
import org.bukkit.Location;

public class CastleBounds {
    HCTeam ownerTeam;
    LocationBounds bounds;
    Location center;

    public CastleBounds(HCTeam ownerTeam, Location minCastle, Location maxCastle) {
        this.ownerTeam = ownerTeam;
        this.bounds = new LocationBounds(minCastle, maxCastle);

        Location min = bounds.getMin();
        Location max = bounds.getMax();

        this.center = new Location(
            bounds.getWorld(),
            0.5d * (min.getX() + max.getX()),
            0.5d * (min.getY() + max.getY()),
            0.5d * (min.getZ() + max.getZ())
        );
    }

    public HCTeam getOwnerTeam() {
        return ownerTeam;
    }

    public LocationBounds getBounds() {
        return bounds;
    }

    public Location getCenter() {
        return center;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(getClass())
            .add("bounds",
                MoreObjects.toStringHelper(LocationBounds.class)
                    .add("min", bounds.getMin())
                    .add("max", bounds.getMax())
                    .toString()
            )
            .add("center", center)
            .toString();

    }


}
