package com.gmail.val59000mc.lostworld.castles;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.lostworld.common.Constants;
import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import io.lumine.xikage.mythicmobs.MythicMobs;
import io.lumine.xikage.mythicmobs.api.bukkit.BukkitAPIHelper;
import io.lumine.xikage.mythicmobs.api.exceptions.InvalidMobTypeException;
import io.lumine.xikage.mythicmobs.mobs.MythicMob;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;

import java.util.List;

public class BridgeTrap {

    private HCGameAPI api;
    private LocationBounds bridge;
    private List<Location> mobsSpawningLocations;
    private boolean hasSpawnedMobs;

    public BridgeTrap(HCGameAPI api, LocationBounds bridge, List<Location> mobsSpawningLocations) {
        super();
        this.api = api;
        this.bridge = bridge;
        this.mobsSpawningLocations = mobsSpawningLocations;
    }

    public void checkForPlayers() {
        boolean hasPlayers = false;
        for (HCPlayer hcPlayer : api.getPlayersManagerAPI().getPlayers(true, PlayerState.PLAYING)) {
            Player player = hcPlayer.getPlayer();
            if (bridge.contains(player.getLocation())) {
                if (!player.hasPotionEffect(PotionEffectType.BLINDNESS)) {
                    api.getSoundAPI().play(hcPlayer, Sound.ENTITY_ZOMBIE_VILLAGER_CURE, 1, 1);
                }
                Effects.addForce(player, PotionEffectType.BLINDNESS, 80, 0);
                Effects.addForce(player, PotionEffectType.SLOW, 80, 0);
                hasPlayers = true;
            }
        }

        // spawn skeletons only once
        if (hasPlayers && !hasSpawnedMobs) {
            BukkitAPIHelper mobApi = MythicMobs.inst().getAPIHelper();
            try {
                MythicMob flyingSkeleton = mobApi.getMythicMob(Constants.FLYING_SKELETON_INTERNAL_NAME);
                for (Location location : mobsSpawningLocations) {
                    mobApi.spawnMythicMob(flyingSkeleton, location, 1);
                }
                hasSpawnedMobs = true;
            } catch (InvalidMobTypeException ex) {
                throw new IllegalStateException("Could not spawn FlyingSkeleton myhtic mob near bridge trap", ex);
            }
        }
    }
}
