package com.gmail.val59000mc.lostworld.objectives.impl;

import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.hcgameslib.tasks.HCTaskScheduler;
import com.gmail.val59000mc.lostworld.castles.BridgeTrap;
import com.gmail.val59000mc.lostworld.castles.CastleBounds;
import com.gmail.val59000mc.lostworld.common.Constants;
import com.gmail.val59000mc.lostworld.events.ObjectiveTeamProgressChangeEvent;
import com.gmail.val59000mc.lostworld.objectives.AbstractGameObjective;
import com.gmail.val59000mc.lostworld.objectives.ObjectiveManager;
import com.gmail.val59000mc.lostworld.objectives.ObjectiveType;
import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;
import com.gmail.val59000mc.lostworld.players.LostWorldTeam;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import com.gmail.val59000mc.spigotutils.Parser;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CastleObjective extends AbstractGameObjective {

    private CastleObjective that = this;
    private HCTaskScheduler playerMoveScheduler;
    private HCTaskScheduler objectiveIncreaseScheduler;
    private HCTaskScheduler bridgeTrapScheduler;
    private Map<LostWorldTeam, CastleBounds> castles;
    private Map<LostWorldTeam, Double> teamProgresses;
    private List<BridgeTrap> bridgeTraps;

    /**
     * Max squared distance allowed between player and castle to break/place a block before reaching castle
     */
    private final static double ALLOWED_SQUARED_DISTANCE_BLOCK_OPERATION = 2000d;

    public CastleObjective(ObjectiveManager manager) {
        super(manager, ObjectiveType.CASTLE);
        this.castles = new HashMap<>();
        this.teamProgresses = new HashMap<>();
        this.bridgeTraps = new ArrayList<>();
        this.playerMoveScheduler = null;
    }

    @Override
    public void start() {
        super.start();
        if (!playerMoveScheduler.isRunning()) {
            playerMoveScheduler.start();
        }
        if (!objectiveIncreaseScheduler.isRunning()) {
            objectiveIncreaseScheduler.start();
        }
        if (!bridgeTrapScheduler.isRunning()) {
            bridgeTrapScheduler.start();
        }
    }

    ;

    @Override
    public void stop() {
        super.stop();
        if (playerMoveScheduler.isRunning()) {
            playerMoveScheduler.stop();
        }
        if (objectiveIncreaseScheduler.isRunning()) {
            objectiveIncreaseScheduler.stop();
        }
        if (bridgeTrapScheduler.isRunning()) {
            bridgeTrapScheduler.stop();
        }
    }

    ;

    @Override
    public int getCompletion(LostWorldTeam team) {
        if (isCompleted() && getCompletedByTeam().equals(team))
            return 1;
        else
            return 0;
    }

    @Override
    public int getCompletionPercentage(LostWorldTeam team) {
        if (!isStarted() && !isCompleted()) {
            return 0;
        } else if (isCompleted() && team.hasReachedCastle()) {
            return 100;
        } else if (isCompleted() && !team.hasReachedCastle()) {
            return 0;
        } else { // started but not completed
            return calculateCompletionDistancePercentage(team);
        }
    }

    private int calculateCompletionDistancePercentage(LostWorldTeam team) {
        Location castleCenter = castles.get(team).getCenter();
        Double maxDistance = getApi().getWorldConfig().getCenter().distance(castleCenter);

        // Searching closed player to castle
        List<HCPlayer> playingMembers = team.getMembers(true, PlayerState.PLAYING);
        Double minDistance = null; // min online team member squared distance to castle
        for (HCPlayer hcPlayer : playingMembers) {
            Double distance = hcPlayer.getPlayer().getLocation().distance(castleCenter);
            if (minDistance == null || distance < minDistance) {
                minDistance = distance;
            }
        }

        Integer progress;
        // Calculating percentage
        if (minDistance == null || minDistance >= maxDistance) {
            progress = 0;
        } else {
            progress = 100 - (int) Math.round(100d * (minDistance / maxDistance));
        }

        return progress;
    }


    @Override
    protected void load() {

        super.load();

        // load castle coordinates

        FileConfiguration cfg = getApi().getConfig();
        World world = getApi().getWorldConfig().getWorld();

        {
            String redMin = cfg.getString("objectives." + getType().name() + ".castles.red.min");
            String redMax = cfg.getString("objectives." + getType().name() + ".castles.red.max");
            LostWorldTeam redTeam = (LostWorldTeam) getPmApi().getHCTeam(Constants.RED_TEAM);
            CastleBounds redCastle = new CastleBounds(redTeam, Parser.parseLocation(world, redMin), Parser.parseLocation(world, redMax));
            castles.put(redTeam, redCastle);
            teamProgresses.put(redTeam, 0d);
            Log.info("Registered red castle for CASTLE objective detection : " + redCastle.toString());

            String greenMin = cfg.getString("objectives." + getType().name() + ".castles.green.min");
            String greenMax = cfg.getString("objectives." + getType().name() + ".castles.green.max");
            LostWorldTeam greenTeam = (LostWorldTeam) getPmApi().getHCTeam(Constants.GREEN_TEAM);
            CastleBounds greenCastle = new CastleBounds(greenTeam, Parser.parseLocation(world, greenMin), Parser.parseLocation(world, greenMax));
            castles.put(greenTeam, greenCastle);
            teamProgresses.put(greenTeam, 0d);
            Log.info("Registered green castle for CASTLE objective detection : " + greenCastle.toString());
        }

        // load trap coordinates
        {
            String redMin = cfg.getString("bridge-traps.red.min");
            String redMax = cfg.getString("bridge-traps.red.max");
            LocationBounds redBounds = new LocationBounds(Parser.parseLocation(world, redMin), Parser.parseLocation(world, redMax));
            List<Location> redLocs = new ArrayList<>();
            List<String> redLocsStr = cfg.getStringList("bridge-traps.red.flying-skeletons");
            if (redLocsStr != null) {
                for (String locStr : redLocsStr) {
                    redLocs.add(Parser.parseLocation(world, locStr));
                }
            }
            bridgeTraps.add(new BridgeTrap(getApi(), redBounds, redLocs));

            String greenMin = cfg.getString("bridge-traps.green.min");
            String greenMax = cfg.getString("bridge-traps.green.max");
            LocationBounds greenBounds = new LocationBounds(Parser.parseLocation(world, greenMin), Parser.parseLocation(world, greenMax));
            List<Location> greenLocs = new ArrayList<>();
            List<String> greenLocsStr = cfg.getStringList("bridge-traps.green.flying-skeletons");
            if (greenLocsStr != null) {
                for (String locStr : greenLocsStr) {
                    greenLocs.add(Parser.parseLocation(world, locStr));
                }
            }
            bridgeTraps.add(new BridgeTrap(getApi(), greenBounds, greenLocs));
        }

        // load player move scheduler
        this.playerMoveScheduler = getApi().buildTask("check player has Reached Castle", new HCTask() {

            @Override
            public void run() {

                for (HCTeam hcTeam : getPmApi().getTeams()) {

                    // Check if a player in the team has reached his castle
                    CastleBounds castle = castles.get(hcTeam);
                    for (HCPlayer hcPlayer : hcTeam.getMembers(true, PlayerState.PLAYING)) {
                        if (!isCompleted() && castle.getBounds().contains(hcPlayer.getPlayer().getLocation())) {
                            LostWorldPlayer lwPlayer = (LostWorldPlayer) hcPlayer;
                            lwPlayer.setReachedCastle();
                            getManager().completeObjective(that, lwPlayer);
                        }
                    }
                }

            }

        })
            .withInterval(20)
            .withIterations(-1)
            .build();

        // load  objective increase scheduler
        this.objectiveIncreaseScheduler = getApi().buildTask("check player has Reached Castle", new HCTask() {

            @Override
            public void run() {

                for (HCTeam hcTeam : getPmApi().getTeams()) {
                    getApi().callEvent(new ObjectiveTeamProgressChangeEvent(getApi(), that, (LostWorldTeam) hcTeam));
                }

            }

        })
            .withInterval(100)
            .withIterations(-1)
            .build();

        // load bridge trap scheduler
        this.bridgeTrapScheduler = getApi().buildTask("check bridge trap", new HCTask() {

            @Override
            public void run() {

                for (BridgeTrap bridgeTrap : bridgeTraps) {
                    bridgeTrap.checkForPlayers();
                }

            }

        })
            .withInterval(30)
            .withIterations(-1)
            .build();

    }


    // prevent breaking on placing block when far from castle
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlaceBlock(BlockPlaceEvent e) {
        if (!isBlockOperationAllowed(e.getPlayer(), e.getBlock().getLocation()))
            e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBreakBlock(BlockBreakEvent e) {
        if (!isBlockOperationAllowed(e.getPlayer(), e.getBlock().getLocation()))
            e.setCancelled(true);
    }


    private boolean isBlockOperationAllowed(Player player, Location location) {
        HCPlayer hcPlayer = getPmApi().getHCPlayer(player);
        if (hcPlayer != null && hcPlayer.hasTeam() && hcPlayer.isPlaying()) {
            CastleBounds bounds = castles.get(hcPlayer.getTeam());
            if (bounds.getCenter().distanceSquared(location) < ALLOWED_SQUARED_DISTANCE_BLOCK_OPERATION)
                return true;
            else
                return false;
        } else {
            return false;
        }
    }


}
