package com.gmail.val59000mc.lostworld.objectives.impl;

import com.gmail.val59000mc.hcgameslib.api.HCBossBarAPI;
import com.gmail.val59000mc.hcgameslib.api.HCStringsAPI;
import com.gmail.val59000mc.hcgameslib.api.impl.HCMessage;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerQuitEvent;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.hcgameslib.tasks.HCTaskScheduler;
import com.gmail.val59000mc.hcgameslib.worlds.WorldConfig;
import com.gmail.val59000mc.lostworld.common.Constants;
import com.gmail.val59000mc.lostworld.events.*;
import com.gmail.val59000mc.lostworld.items.LostWorldtems;
import com.gmail.val59000mc.lostworld.objectives.AbstractGameObjective;
import com.gmail.val59000mc.lostworld.objectives.ObjectiveManager;
import com.gmail.val59000mc.lostworld.objectives.ObjectiveType;
import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;
import com.gmail.val59000mc.lostworld.players.LostWorldTeam;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.ItemCompareOption;
import com.gmail.val59000mc.spigotutils.Parser;
import io.lumine.xikage.mythicmobs.MythicMobs;
import io.lumine.xikage.mythicmobs.api.bukkit.BukkitAPIHelper;
import io.lumine.xikage.mythicmobs.api.bukkit.events.MythicMobDeathEvent;
import io.lumine.xikage.mythicmobs.api.exceptions.InvalidMobTypeException;
import io.lumine.xikage.mythicmobs.mobs.MythicMob;
import org.bukkit.*;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.block.Block;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.util.BoundingBox;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class BobObjective extends AbstractGameObjective {

    /**
     * Scheduler to emit objective increase events
     */
    HCTaskScheduler objectiveIncreaseScheduler;

    /**
     * Scheduler to emit objective increase events
     */
    HCTaskScheduler objectiveFireworkScheduler;

    /**
     * Scheduler to show bob health is a boss bar
     */
    HCTaskScheduler objectiveBossBarScheduler;

    /**
     * Castles centers
     */
    Map<LostWorldTeam, Location> castles;

    /**
     * Player who has BOB head in his inventory
     */
    private LostWorldPlayer bobItemHolder;

    /**
     * Location where bob will spawn
     */
    private Location bobSpawningLocation;

    /**
     * Stores Bobb uuid when spawned, null before
     */
    private UUID bobUuuid;

    private double bobMaxHealth;

    private boolean protectSpawningLocation;
    private static double MAX_SQUARED_DISTANCE_SPAWNPING_PROTECTION = 256;

    /**
     * Bob item if on ground
     */
    private Item bobGroundItem;

    public BobObjective(ObjectiveManager manager) {
        super(manager, ObjectiveType.BOB);
        this.bobItemHolder = null;
        this.bobGroundItem = null;
        this.protectSpawningLocation = true;
        this.castles = new HashMap<>();
        this.bobUuuid = null;
    }

    @Override
    public void start() {
        super.start();
    }

    ;

    @Override
    public void stop() {
        super.stop();
        if (objectiveIncreaseScheduler.isRunning()) {
            objectiveIncreaseScheduler.stop();
        }
        if (objectiveFireworkScheduler.isRunning()) {
            objectiveFireworkScheduler.stop();
        }
        if (objectiveBossBarScheduler.isRunning()) {
            objectiveBossBarScheduler.stop();
        }
    }

    ;

    @Override
    public int getCompletion(LostWorldTeam team) {
        if (isCompleted() && getCompletedByTeam().equals(team))
            return 1;
        else
            return 0;
    }

    @Override
    public int getCompletionPercentage(LostWorldTeam team) {
        if (!isStarted() && !isCompleted()) {
            return 0;
        } else if (isCompleted() && team.hasTakenBob()) {
            return 100;
        } else if (isCompleted() && !team.hasTakenBob()) {
            return 0;
        } else { // started but not completed
            return calculateCompletionDistancePercentage(team);
        }
    }

    /**
     * Progress is represented by the distance between the player who holds BoB head and its castle
     * If the item is on the ground or the head is hold by the other team, the progress is 0
     *
     * @param team
     * @return
     */
    private int calculateCompletionDistancePercentage(LostWorldTeam team) {
        if (bobItemHolder == null || !bobItemHolder.isOnline() || !bobItemHolder.getTeam().equals(team))
            return 0;


        Location castleCenter = castles.get(team);
        Double maxDistance = getApi().getWorldConfig().getCenter().distance(castleCenter);

        // Calculating distance from holder to castle
        Double distance = bobItemHolder.getPlayer().getLocation().distance(castleCenter);

        // Calculating percentage
        if (distance == null || distance >= maxDistance) {
            return 0;
        } else {
            return 100 - (int) Math.round(100d * (distance / maxDistance));
        }
    }

    @Override
    protected void load() {
        super.load();

        // load castle coordinates
        FileConfiguration cfg = getApi().getConfig();
        WorldConfig worldConfig = getApi().getWorldConfig();
        World world = worldConfig.getWorld();

        Location redCastle = Parser.parseLocation(world, cfg.getString("objectives." + getType().name() + ".castles.red"));
        castles.put((LostWorldTeam) getPmApi().getHCTeam(Constants.RED_TEAM), redCastle);
        Log.info("Registered red castle for CASTLE objective detection : " + redCastle.toString());

        Location greenCastle = Parser.parseLocation(world, cfg.getString("objectives." + getType().name() + ".castles.green"));
        castles.put((LostWorldTeam) getPmApi().getHCTeam(Constants.GREEN_TEAM), greenCastle);
        Log.info("Registered green castle for CASTLE objective detection : " + greenCastle.toString());

        bobSpawningLocation = Parser.parseLocation(world, cfg.getString("objectives." + getType().name() + ".spawning-location"));

        // load
        this.objectiveIncreaseScheduler = getApi().buildTask("check player has Reached Castle", new HCTask() {

            @Override
            public void run() {

                for (HCTeam hcTeam : getPmApi().getTeams()) {
                    getApi().callEvent(new ObjectiveTeamProgressChangeEvent(getApi(), BobObjective.this, (LostWorldTeam) hcTeam));
                }

            }

        })
            .withInterval(100)
            .withIterations(-1)
            .build();

        this.objectiveFireworkScheduler = getApi().buildTask("spawn firework at bob item position", new HCTask() {

            @Override
            public void run() {

                Location fireworkLocation = null;

                if (bobGroundItem != null && bobGroundItem.isValid()) {
                    fireworkLocation = bobGroundItem.getLocation();
                    bobGroundItem.setTicksLived(1); // prevent item despawn
                } else if (bobItemHolder != null && bobItemHolder.isOnline() && LostWorldtems.hasBobHead(bobItemHolder.getPlayer())) {
                    fireworkLocation = bobItemHolder.getPlayer().getLocation();
                }
                if (fireworkLocation != null) {

                    Firework fw = (Firework) fireworkLocation.getWorld().spawnEntity(fireworkLocation, EntityType.FIREWORK);
                    FireworkMeta fwm = fw.getFireworkMeta();

                    Color secondaryColor = Color.fromRGB(255, 255, 255);

                    //Create our effect with this
                    FireworkEffect effect = FireworkEffect.builder()
                        .with(Type.BALL_LARGE)
                        .flicker(true)
                        .withColor(Color.YELLOW, secondaryColor)
                        .withFade(Color.YELLOW, secondaryColor)
                        .build();

                    fwm.addEffect(effect);
                    fwm.setPower(2);
                    fw.setFireworkMeta(fwm);
                }


            }

        })
            .withInterval(100)
            .withIterations(-1)
            .build();


        BoundingBox bbox = BoundingBox.of(worldConfig.getCenter(), 200, 200, 200);
        HCStringsAPI stringsAPI = getApi().getStringsAPI();
        HCBossBarAPI bossBarAPI = stringsAPI.getBossBarAPI();
        BossBar bossBar = bossBarAPI.createBossBar(BarColor.YELLOW, BarStyle.SEGMENTED_10);
        this.objectiveBossBarScheduler = getApi().buildTask("bob health in boss bar", (task) -> {
            world.getNearbyEntities(bbox, e -> e.getUniqueId().equals(bobUuuid) && e instanceof LivingEntity)
                .stream()
                .findFirst()
                .ifPresent(entity -> {
                    LivingEntity bob = (LivingEntity) entity;
                    double bobHealth = bob.getHealth();
                    double bobHealthRatio = BigDecimal.valueOf(bobMaxHealth <= 0d ? 1.d : Math.max(0d, bobHealth / bobMaxHealth))
                        .setScale(2, RoundingMode.HALF_UP)
                        .doubleValue();
                    HCMessage hcMessage = stringsAPI.get("lostworld.objectives." + getType().name() + ".bob-health")
                        .replace("%healthRatio%", String.valueOf((int) (bobHealthRatio * 100)));
                    bossBarAPI.updateBossBar(bossBar, bobHealthRatio);
                    bossBarAPI.sendBossBar(bossBar, hcMessage);
                });
        })
            .withOnStop(task -> {
                bossBarAPI.deleteBossBar(bossBar);
            })
            .withInterval(20)
            .withIterations(-1)
            .build();

    }


    @EventHandler
    public void onTriggerBobSpawn(BobSpawnEvent e) {
        BukkitAPIHelper mobApi = MythicMobs.inst().getAPIHelper();

        if (!objectiveIncreaseScheduler.isRunning()) {
            objectiveIncreaseScheduler.start();
        }
        if (!objectiveFireworkScheduler.isRunning()) {
            objectiveFireworkScheduler.start();
        }
        if (!objectiveBossBarScheduler.isRunning()) {
            objectiveBossBarScheduler.start();
        }

        protectSpawningLocation = false;

        // Build locations where to spawn explosions
        List<Location> tntLocations = new ArrayList<>();
        World world = bobSpawningLocation.getWorld();
        for (int i = -6; i <= 6; i += 4) {
            for (int j = -7; j <= 7; j += 7) {
                tntLocations.add(bobSpawningLocation.clone().add(i, 6, j));
            }
        }
        Collections.shuffle(tntLocations);

        getApi().buildTask("explode ground before spawn bob", (task) -> {
            if (tntLocations.isEmpty()) {
                try {
                    MythicMob bob = mobApi.getMythicMob(Constants.BOB_INTERNAL_NAME);
                    Entity bobEntity = mobApi.spawnMythicMob(bob, bobSpawningLocation, 1);
                    bobUuuid = bobEntity.getUniqueId();
                    if (bobEntity instanceof LivingEntity) {
                        LivingEntity bobLivingEntity = (LivingEntity) bobEntity;
                        AttributeInstance maxHealth = bobLivingEntity.getAttribute(Attribute.GENERIC_MAX_HEALTH);
                        bobMaxHealth = maxHealth == null ? 0d : maxHealth.getValue();

                    }
                } catch (InvalidMobTypeException ex) {
                    throw new IllegalStateException("Could not spawn Bob mythic mob", ex);
                }
                task.stop();
            } else {
                Location tntLoc = tntLocations.remove(0);
                world.createExplosion(tntLoc, 5);
                world.strikeLightningEffect(tntLoc);
                getApi().getSoundAPI().play(Sound.BLOCK_NOTE_BLOCK_PLING, 1, 2);
            }
        })
            .withInterval(5)
            .build()
            .start();


        getApi().getSoundAPI().play(Sound.ENTITY_WITHER_SPAWN, 2, 2);
        getApi().getStringsAPI().get("lostworld.objectives." + getType().name() + ".bob-spawn-title")
            .sendTitle(20, 80, 20);
        getApi().getStringsAPI().get("lostworld.objectives." + getType().name() + ".bob-spawn")
            .sendChatP();


    }


    @EventHandler
    public void onBobDeath(MythicMobDeathEvent e) {
        if (e.getMobType().getInternalName().equals(Constants.BOB_INTERNAL_NAME)) {
            LivingEntity entity = (LivingEntity) e.getEntity();

            if (objectiveBossBarScheduler.isRunning()) {
                objectiveBossBarScheduler.stop();
            }

            // drop BoB head on death location
            Location deathLocation = entity.getLocation();
            bobGroundItem = deathLocation.getWorld().dropItem(deathLocation, LostWorldtems.getBobHead());

            Player killer = entity.getKiller();

            if (killer == null) {
                getApi().getStringsAPI().get("lostworld.objectives." + getType().name() + ".bob-death").sendChatP();
            } else {
                LostWorldPlayer lwPlayer = (LostWorldPlayer) getApi().getPlayersManagerAPI().getHCPlayer(killer);
                getApi().getStringsAPI()
                    .get("lostworld.objectives." + getType().name() + ".bob-death-player")
                    .replace("%player%", lwPlayer.getColoredName())
                    .sendChatP();

                double bobKillReward = getApi().getConfig().getDouble("objectives.BOB.rewards.on-bob-kill", Constants.BOB_KILL_REWARD);
                getApi().getPlayersManagerAPI().rewardMoneyTo(lwPlayer, bobKillReward);
            }

            getApi().getStringsAPI().get("lostworld.objectives." + getType().name() + ".bob-death-title")
                .sendTitle(20, 60, 20);
        }
    }

    @EventHandler
    public void onBobCollected(BobCollectedEvent e) {
        LostWorldPlayer lwPlayer = e.getLostWorldPlayer();
        if (lwPlayer.hasTeam() && lwPlayer.isPlaying()) {
            if (!isCompleted()) {
                lwPlayer.setTakenBob();
                getManager().completeObjective(this, lwPlayer);
            }
        }
    }

    @EventHandler
    public void onPickUpBobItem(EntityPickupItemEvent e) {
        if (e.getEntity() instanceof Player) {
            Player player = (Player) e.getEntity();
            if (LostWorldtems.isBobHead(e.getItem())) {
                bobGroundItem = null;
                bobItemHolder = (LostWorldPlayer) getPmApi().getHCPlayer(player);
                getApi().callEvent(new BobPickedUpEvent(getApi(), bobItemHolder));
            }
        } else {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onDeathWithBobItemInInventory(PlayerDeathEvent e) {
        Iterator<ItemStack> it = e.getDrops().iterator();
        while (it.hasNext()) {
            ItemStack item = it.next();
            if (LostWorldtems.isBobHead(item)) {
                it.remove();
                LostWorldPlayer lwPlayer = (LostWorldPlayer) getPmApi().getHCPlayer(e.getEntity());
                bobItemHolder = null;
                bobGroundItem = e.getEntity().getWorld().dropItem(e.getEntity().getLocation(), LostWorldtems.getBobHead());
                getApi().callEvent(new BobDropedEvent(getApi(), lwPlayer));
            }
        }
    }

    @EventHandler
    public void onDropBobHead(PlayerDropItemEvent e) {
        ItemStack stack = e.getItemDrop().getItemStack();
        if (LostWorldtems.isBobHead(stack)) {
            LostWorldPlayer lwPlayer = (LostWorldPlayer) getPmApi().getHCPlayer(e.getPlayer());
            bobItemHolder = null;
            bobGroundItem = e.getItemDrop();
            getApi().callEvent(new BobDropedEvent(getApi(), lwPlayer));
        }
    }

    @EventHandler
    public void onHolderDisconnect(HCPlayerQuitEvent e) {
        if (e.getHcPlayer().equals(bobItemHolder)) {
            Player player = e.getHcPlayer().getPlayer();
            Inventories.remove(
                player.getInventory(),
                LostWorldtems.getBobHead(),
                new ItemCompareOption.Builder()
                    .withAmount(false)
                    .withDamageValue(true)
                    .withDisplayName(true)
                    .withEnchantments(true)
                    .withLore(true)
                    .withMaterial(true)
                    .build()
            );
            bobItemHolder = null;
            bobGroundItem = player.getWorld().dropItem(player.getLocation(), LostWorldtems.getBobHead());
            getApi().callEvent(new BobDropedEvent(getApi(), (LostWorldPlayer) e.getHcPlayer()));
        }
    }

    // prevent Shift click head from your inventory into the chest
    @EventHandler
    public void onInventoryShiftClick(InventoryClickEvent event) {
        if (event.getClick().isShiftClick()) {
            Inventory clicked = event.getClickedInventory();
            if (clicked == event.getWhoClicked().getInventory()) {
                // The item is being shift clicked from the bottom to the top
                ItemStack clickedOn = event.getCurrentItem();

                if (clickedOn != null && LostWorldtems.isBobHead(clickedOn)) {
                    event.setCancelled(true);
                }
            }
        }
    }

    // prevent Click the head, and then click it into the slot in the chest
    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Inventory clicked = event.getClickedInventory();
        if (clicked != event.getWhoClicked().getInventory()) { // Note: !=
            // The cursor item is going into the top inventory
            ItemStack onCursor = event.getCursor();

            if (onCursor != null && LostWorldtems.isBobHead(onCursor)) {
                event.setCancelled(true);
            }
        }
    }

    // prevent Click the item, and drag it inside the chest
    @EventHandler
    public void onInventoryDrag(InventoryDragEvent event) {
        ItemStack dragged = event.getOldCursor(); // This is the item that is being dragged

        if (LostWorldtems.isBobHead(dragged)) {
            int inventorySize = event.getInventory().getSize(); // The size of the inventory, for reference

            // Now we go through all of the slots and check if the slot is inside our inventory (using the inventory size as reference)
            for (int i : event.getRawSlots()) {
                if (i < inventorySize) {
                    event.setCancelled(true);
                    break;
                }
            }
        }
    }

    // prevent hopper picking up the head and insert it into the chest
    @EventHandler
    public void onHopperPickUp(InventoryPickupItemEvent e) {
        if (LostWorldtems.isBobHead(e.getItem())) {
            e.setCancelled(true);
        }
    }

    // prevent hopper picking up the head and insert it into the chest
    @EventHandler
    public void onHopperMove(InventoryMoveItemEvent e) {
        if (LostWorldtems.isBobHead(e.getItem())) {
            e.setCancelled(true);
        }
    }

    // prevent head damage and teleport to center if falling in the void
    @EventHandler
    public void onHeadDamage(EntityDamageEvent e) {
        if (e.getEntityType().equals(EntityType.DROPPED_ITEM)) {
            Item item = (Item) e.getEntity();
            if (LostWorldtems.isBobHead(item.getItemStack())) {
                e.setCancelled(true);
            }
        }
    }

    // prevent placing head as a block
    @EventHandler
    public void onPlaceBlock(BlockPlaceEvent e) {
        if (LostWorldtems.isBobHead(e.getPlayer().getItemInHand())) {
            e.setCancelled(true);
            getStringsApi()
                .get("lostworld.objectives." + getType().name() + ".cannot-place-block")
                .sendChat(getPmApi().getHCPlayer(e.getPlayer()));
        }
    }

    // protect spawning location before bob spawn
    @EventHandler(ignoreCancelled = true)
    public void onPlaceBlockInBobSpawningZone(BlockPlaceEvent e) {
        if (protectSpawningLocation) {
            if (e.getBlock().getLocation().distanceSquared(bobSpawningLocation) < MAX_SQUARED_DISTANCE_SPAWNPING_PROTECTION) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onBreakBlockInBobSpawningZone(BlockBreakEvent e) {
        if (protectSpawningLocation) {
            if (e.getBlock().getLocation().distanceSquared(bobSpawningLocation) < MAX_SQUARED_DISTANCE_SPAWNPING_PROTECTION) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onBlockExplodeInBobSpawningZone(BlockExplodeEvent e) {
        if (protectSpawningLocation) {
            for (Block block : e.blockList()) {
                if (block.getLocation().distanceSquared(bobSpawningLocation) < MAX_SQUARED_DISTANCE_SPAWNPING_PROTECTION) {
                    e.setCancelled(true);
                    break;
                }
            }
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onBlockExplodeInBobSpawningZone(EntityExplodeEvent e) {
        if (protectSpawningLocation) {
            for (Block block : e.blockList()) {
                if (block.getLocation().distanceSquared(bobSpawningLocation) < MAX_SQUARED_DISTANCE_SPAWNPING_PROTECTION) {
                    e.setCancelled(true);
                    break;
                }
            }
        }
    }

}
