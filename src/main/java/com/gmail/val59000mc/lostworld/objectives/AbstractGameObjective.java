package com.gmail.val59000mc.lostworld.objectives;

import org.bukkit.configuration.file.FileConfiguration;

import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;
import com.gmail.val59000mc.lostworld.players.LostWorldTeam;

public abstract class AbstractGameObjective extends HCListener implements GameObjective{

	
	private ObjectiveManager manager;
	
	/**
	 * Objective type
	 */
	private ObjectiveType type;
	
	/**
	 * Is objective tracking team scores ?
	 */
	private boolean started;
	
	/**
	 * Score given to the team who complete the event
	 */
	private int score;
	
	/**
	 * Number of times the objective has to be incremented to be completed
	 * e.g: for a kills objective of 30
	 */
	private int limit;
	
	/**
	 * Team who completed the objective
	 * The attribute remains null until the objective is completed
	 */
	private LostWorldTeam team;
	
	/**
	 * Team reward on completion in HC
	 */
	private double teamReward;
	
	/**
	 * Player who acutally completed the team
	 * e.g : last kill for a kills objective
	 */
	private LostWorldPlayer player;
	
	/**
	 * Player reward on completion in HC
	 */
	private double playerReward;

	///////////////////////////////
	// Constructor
	//////////////////////////
	
	public AbstractGameObjective(ObjectiveManager manager, ObjectiveType type){
		this.manager = manager;
		this.type = type;
		this.score = 0;
		this.limit = 0;
		this.started = false;
	}

	///////////////////////////////
	// AbstractGameObjective
	//////////////////////////
	
	
	/**
	 * Register objective tracking
	 */
	public void start(){
		getApi().registerListener(this);
		this.started = true;
	}

	/**
	 * Unregister objective tracking
	 */
	public void stop(){
		getApi().unregisterListener(this);
		this.started = false;
	}
	
	public ObjectiveType getType(){
		return this.type;
	}

	public void setTeamReward(double teamReward) {
		this.teamReward = teamReward;
	}

	public void setPlayerReward(double playerReward) {
		this.playerReward = playerReward;
	}
	
	protected ObjectiveManager getManager(){
		return manager;
	}
	

	public void setCompletedByTeam(LostWorldTeam team) {
		this.team = team;
	}
	

	public void setCompletedByPlayer(LostWorldPlayer player) {
		this.player = player;
	}
	
	protected void load(){
		FileConfiguration cfg = getApi().getConfig();
		this.score = cfg.getInt("objectives."+getType().name()+".score",0);
		this.limit = cfg.getInt("objectives."+getType().name()+".limit",1);
		this.teamReward = cfg.getDouble("objectives."+getType().name()+".rewards.team",0);
		this.playerReward = cfg.getDouble("objectives."+getType().name()+".rewards.player",0);
	}
	
	/**
	 * Get best team whether the objective has been completed or not
	 * When the objective is not completed, a team has to be online to be considered the best team
	 * @return
	 */
	public LostWorldTeam getBestTeam(){
		if(isCompleted()){
			return team;
		}
		
		LostWorldTeam bestTeam = null;
		boolean isSameScore = false;
		for(HCTeam team : getPmApi().getTeams()){
			LostWorldTeam lwTeam = (LostWorldTeam) team;
			if(lwTeam.isAsLeastOnlineAnd(PlayerState.PLAYING)){
				if(getCompletionPercentage(lwTeam) > 0){
					if(bestTeam == null){
						bestTeam = lwTeam;
					}else{
						if(getCompletionPercentage(lwTeam) == getCompletionPercentage(bestTeam)){
							isSameScore = true;
						}else if(getCompletionPercentage(lwTeam) > getCompletionPercentage(bestTeam)){
							isSameScore = false;
							bestTeam = lwTeam;
						}
					}
				}
			}
		}
		if(isSameScore){
			return null;
		}
		
		return bestTeam;
	}
	
	/////////////////////////////
	// GameObjective interface
	//////////////////////////
	
	@Override
	public boolean isCompleted() {
		return team != null;
	}

	@Override
	public boolean isStarted() {
		return started;
	}

	@Override
	public LostWorldTeam getCompletedByTeam() {
		return team;
	}

	@Override
	public double getTeamReward() {
		return teamReward;
	}
	
	@Override
	public LostWorldPlayer getCompletedByPlayer() {
		return player;
	}

	@Override
	public double getPlayerReward() {
		return playerReward;
	}


	@Override
	public int getScore() {
		return score;
	}

	@Override
	public int getLimit() {
		return limit;
	}
	
	
}
