package com.gmail.val59000mc.lostworld.objectives.impl;

import com.gmail.val59000mc.lostworld.common.Constants;
import com.gmail.val59000mc.lostworld.events.ObjectivePlayerProgressChangeEvent;
import com.gmail.val59000mc.lostworld.objectives.AbstractGameObjective;
import com.gmail.val59000mc.lostworld.objectives.ObjectiveManager;
import com.gmail.val59000mc.lostworld.objectives.ObjectiveType;
import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;
import com.gmail.val59000mc.lostworld.players.LostWorldTeam;
import com.gmail.val59000mc.spigotutils.Parser;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TowersObjective extends AbstractGameObjective {

    private double obsidianReward;

    private Map<LostWorldTeam, List<Location>> obsidians;


    public TowersObjective(ObjectiveManager manager) {
        super(manager, ObjectiveType.TOWERS);
        this.obsidians = new HashMap<>();
    }

    @Override
    public int getCompletion(LostWorldTeam team) {
        return team.getObsidiansBroken();
    }

    @Override
    public int getCompletionPercentage(LostWorldTeam team) {
        return (int) Math.round(100d * ((double) team.getObsidiansBroken() / (double) getLimit()));
    }

    @Override
    protected void load() {
        super.load();
        this.obsidianReward = getApi().getConfig().getDouble("objectives." + getType().name() + ".rewards.per-obsi", 0);

        this.obsidians.put((LostWorldTeam) getPmApi().getHCTeam(Constants.RED_TEAM), loadObsidianToBreak("red"));
        this.obsidians.put((LostWorldTeam) getPmApi().getHCTeam(Constants.GREEN_TEAM), loadObsidianToBreak("green"));

    }

    private List<Location> loadObsidianToBreak(String teamCfgName) {
        World world = getApi().getWorldConfig().getWorld();

        List<Location> obsidianToBreak = new ArrayList<>();
        for (String locStr : getApi().getConfig().getStringList("objectives." + getType().name() + ".obsidians-to-break." + teamCfgName)) {
            Location loc = Parser.parseLocation(world, locStr);
            // look around and pick obsidian locations blocks
            for (int i = -3; i <= 3; i++) {
                for (int j = -3; j <= 3; j++) {
                    for (int k = -3; k <= 3; k++) {
                        Location around = toBlockLoc(loc.clone().add(i, j, k));
                        if (around.getBlock().getType().equals(Material.OBSIDIAN)) {
                            obsidianToBreak.add(around);
                        }

                    }
                }
            }
        }

        return obsidianToBreak;
    }

    private Location toBlockLoc(Location location) {
        Location asBlock = location.clone();
        asBlock.setX(asBlock.getBlockX());
        asBlock.setY(asBlock.getBlockY());
        asBlock.setZ(asBlock.getBlockZ());
        asBlock.setPitch(0);
        asBlock.setYaw(0);
        return asBlock;
    }

    @EventHandler
    public void onPlaceObsidian(BlockPlaceEvent e) {
        if (e.getBlock().getType().equals(Material.OBSIDIAN)) {
            e.setCancelled(true);
        }
    }

    @SuppressWarnings("deprecation")
    @EventHandler
    public void obsidianGenerators(BlockFromToEvent event) {
        Block block = event.getToBlock();
        Material source = event.getBlock().getType();
        Material target = block.getType();
        if ((target == Material.REDSTONE_WIRE || target == Material.TRIPWIRE)
            && (source == Material.AIR || source == Material.LAVA)) {
            block.setType(Material.AIR);
        }
    }


    @EventHandler
    public void onBreakTowerObsidian(BlockBreakEvent e) {
        Block block = e.getBlock();
        if (block.getType().equals(Material.OBSIDIAN)) {
            LostWorldPlayer lwPlayer = (LostWorldPlayer) getPmApi().getHCPlayer(e.getPlayer());
            if (lwPlayer != null && lwPlayer.hasTeam() && lwPlayer.isPlaying()) {
                Location blockLoc = toBlockLoc(block.getLocation());
                if (obsidians.get(lwPlayer.getTeam()).contains(blockLoc)) {
                    block.setType(Material.AIR);
                    getSoundApi().play(Sound.ENTITY_ZOMBIE_BREAK_WOODEN_DOOR, 2, 1);
                    getStringsApi()
                        .get("lostworld.objectives." + getType().name() + ".obsi-broken")
                        .replace("%player%", lwPlayer.getColoredName())
                        .sendChatP();
                    obsidians.get(lwPlayer.getTeam()).remove(blockLoc);
                    lwPlayer.addObsidiansBroken();
                    getPmApi().rewardMoneyTo(lwPlayer, obsidianReward);
                    getApi().callEvent(new ObjectivePlayerProgressChangeEvent(getApi(), this, lwPlayer));
                    checkObjectiveCompleted(lwPlayer);
                } else {
                    getStringsApi().get("lostworld.objectives." + getType().name() + ".other-tower").sendChat(lwPlayer);
                }
            }

            e.setCancelled(true);
        }
    }

    private void checkObjectiveCompleted(LostWorldPlayer lwPlayer) {
        if (!isCompleted() && ((LostWorldTeam) lwPlayer.getTeam()).getObsidiansBroken() >= getLimit()) {
            getManager().completeObjective(this, lwPlayer);
        }
    }

}
