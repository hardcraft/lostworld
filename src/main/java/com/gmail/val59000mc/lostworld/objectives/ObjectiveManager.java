package com.gmail.val59000mc.lostworld.objectives;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCAfterStartEvent;
import com.gmail.val59000mc.hcgameslib.events.HCBeforeEndEvent;
import com.gmail.val59000mc.hcgameslib.game.GameState;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.lostworld.events.ObjectiveCompletedEvent;
import com.gmail.val59000mc.lostworld.events.ObjectiveForceCompletedEvent;
import com.gmail.val59000mc.lostworld.objectives.impl.BobObjective;
import com.gmail.val59000mc.lostworld.objectives.impl.CastleObjective;
import com.gmail.val59000mc.lostworld.objectives.impl.KillsObjective;
import com.gmail.val59000mc.lostworld.objectives.impl.TowersObjective;
import com.gmail.val59000mc.lostworld.objectives.impl.TrophiesObjective;
import com.gmail.val59000mc.lostworld.objectives.impl.ZombiesObjective;
import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;
import com.gmail.val59000mc.lostworld.players.LostWorldTeam;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.Randoms;

public class ObjectiveManager extends HCListener{

	private Map<ObjectiveType, AbstractGameObjective> objectives;

	public ObjectiveManager(HCGameAPI api){
		setApi(api);
		this.objectives = new HashMap<>();
		load();
	}

	public GameObjective getObjective(ObjectiveType type){
		return objectives.get(type);
	}
	
	///////////////////
	// load
	///////////////
	
	private void load() {
		registerObjective(new CastleObjective(this));
		registerObjective(new ZombiesObjective(this));
		registerObjective(new KillsObjective(this));
		registerObjective(new BobObjective(this));
		registerObjective(new TrophiesObjective(this));
		registerObjective(new TowersObjective(this));
		
		getApi().registerListener(this);
	}

	private void registerObjective(AbstractGameObjective objective) {
		objective.setApi(getApi());
		objective.load();
		objectives.put(objective.getType(), objective);
	}
	
	
	//////////////////////////
	// API
	///////////////////////////
	
	public void forceCompleteAllUncompletedObjectives(){
		
		for(GameObjective obj : objectives.values()){
			if(!obj.isCompleted()){
				LostWorldTeam bestTeam = ((AbstractGameObjective) obj).getBestTeam();
				if(bestTeam != null){
					
					List<HCPlayer> members = bestTeam.getMembers(true, PlayerState.PLAYING);
					LostWorldPlayer randomLwPlayer = (LostWorldPlayer) members.get(Randoms.randomInteger(0, members.size()-1));
					
					completeObjective(obj, randomLwPlayer, true);
					
				}
			}
		}
	}

	private void completeObjective(GameObjective objective, LostWorldPlayer lwPlayer, boolean force) {
		if(objective.isStarted() && !objective.isCompleted() && lwPlayer.hasTeam() && lwPlayer.isPlaying()){

			AbstractGameObjective obj = ((AbstractGameObjective) objective);
			obj.setCompletedByPlayer((LostWorldPlayer) lwPlayer);
			obj.setCompletedByTeam((LostWorldTeam) lwPlayer.getTeam());
			lwPlayer.addScore(obj.getScore());
			
			// reward money
			getPmApi().rewardMoneyTo(lwPlayer, objective.getPlayerReward());
			for(HCPlayer hcPlayer : lwPlayer.getTeam().getOtherMembers(lwPlayer)){
				getPmApi().rewardMoneyTo(hcPlayer, objective.getTeamReward());
			}
			
			obj.stop();
			
			if(force)
				getApi().callEvent(new ObjectiveForceCompletedEvent(getApi(), objective));
			else
				getApi().callEvent(new ObjectiveCompletedEvent(getApi(), objective));
			
		}
	}
	
	public void completeObjective(GameObjective objective, LostWorldPlayer lwPlayer) {
		completeObjective(objective, lwPlayer, false);
	}
	
	///////////////
	// Listener
	///////////////
	
	// Start castle objective on game start
	@EventHandler
	public void onGameStart(HCAfterStartEvent e){
		((AbstractGameObjective) getObjective(ObjectiveType.CASTLE)).start();
	}
	
	// Start other objectives upon castle objective completion
	@EventHandler(priority=EventPriority.HIGH)
	public void onCastleObjectiveCompleted(ObjectiveCompletedEvent e){
		if(e.getObjective().getType().equals(ObjectiveType.CASTLE)){
			((AbstractGameObjective) getObjective(ObjectiveType.BOB)).start();
			((AbstractGameObjective) getObjective(ObjectiveType.KILLS)).start();
			((AbstractGameObjective) getObjective(ObjectiveType.TOWERS)).start();
			((AbstractGameObjective) getObjective(ObjectiveType.TROPHIES)).start();
			((AbstractGameObjective) getObjective(ObjectiveType.ZOMBIES)).start();
			
			// teleport players to their castle once the castle objective is completed
			for(HCTeam team : getPmApi().getTeams()){
				LostWorldTeam lwTeam = (LostWorldTeam) team;
				lwTeam.moveToCastleSpawnpoint();
				Location castleSpawnpoint = team.getSpawnpoint();
				for(HCPlayer hcPlayer : team.getMembers(null,  PlayerState.PLAYING)){
					hcPlayer.setStuff(lwTeam.getStuff());
					if(hcPlayer.isOnline()){
						Player player = hcPlayer.getPlayer();
						Inventories.clear(player);
						getItemsApi().giveStuffItemsToPlayer(hcPlayer);
						hcPlayer.getPlayer().teleport(castleSpawnpoint);
					}
					
				}
			}
		}
	}
	

	@EventHandler
	public void onAllObjectiveCompleted(ObjectiveCompletedEvent e){
		boolean allCompleted = true;
		for(AbstractGameObjective obj : objectives.values()){
			if(!obj.isCompleted())
				allCompleted = false;
		}
		if(allCompleted){
			getApi().sync(()->{
				if(getApi().is(GameState.PLAYING)){
					getStringsApi().get("lostworld.objectives.all-completed").sendChatP();
					getPmApi().forceDeclareWinner();
				}
			}, 50);
		}
		
	}
	
	// Stop all objectives when game ends
	@EventHandler
	public void onGameEnd(HCBeforeEndEvent e){
		for(AbstractGameObjective obj : objectives.values()){
			obj.stop();
		}
	}
}
