package com.gmail.val59000mc.lostworld.objectives.impl;

import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockPlaceEvent;

import com.gmail.val59000mc.lostworld.events.ObjectivePlayerProgressChangeEvent;
import com.gmail.val59000mc.lostworld.events.TrophyCollectedEvent;
import com.gmail.val59000mc.lostworld.items.LostWorldtems;
import com.gmail.val59000mc.lostworld.objectives.AbstractGameObjective;
import com.gmail.val59000mc.lostworld.objectives.ObjectiveManager;
import com.gmail.val59000mc.lostworld.objectives.ObjectiveType;
import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;
import com.gmail.val59000mc.lostworld.players.LostWorldTeam;

public class TrophiesObjective extends AbstractGameObjective{
	
	private double trophyReward;
	
	public TrophiesObjective(ObjectiveManager manager) {
		super(manager, ObjectiveType.TROPHIES);
	}

	@Override
	public int getCompletion(LostWorldTeam team) {
		return team.getTrophiesCollected();
	}

	@Override
	public int getCompletionPercentage(LostWorldTeam team) {
		return (int) Math.round(100d * ((double) team.getTrophiesCollected() / (double) getLimit()));
	}

	@Override
	protected void load() {
		super.load();
		this.trophyReward = getApi().getConfig().getDouble("objectives."+getType().name()+".rewards.per-trophy",0);
	}

	// deny placing trophy item on ground
	@EventHandler
	public void onPlaceTrophyBlock(BlockPlaceEvent e){

		if(LostWorldtems.isTrophyItem(e.getItemInHand())){ 
			e.setCancelled(true);
			LostWorldPlayer lwPlayer = (LostWorldPlayer) getPmApi().getHCPlayer(e.getPlayer());
			if(lwPlayer != null && lwPlayer.isPlaying()){
				getStringsApi()
					.get("lostworld.objectives."+getType().name()+".cannot-place-block")
					.sendChat(lwPlayer);
			}
		}
		
	}
	
	@EventHandler
	public void onTrophyCollected(TrophyCollectedEvent e){
		LostWorldPlayer lwPlayer = e.getLostWorldPlayer();
		if(lwPlayer.hasTeam() && lwPlayer.isPlaying()){
			int trophiesToAdd = e.getAmount();
			int teamTrophies = ((LostWorldTeam) lwPlayer.getTeam()).getTrophiesCollected();
			int maxTrophiesToAdd = getLimit() - teamTrophies;
			if(trophiesToAdd > maxTrophiesToAdd){
				trophiesToAdd = maxTrophiesToAdd;
			}
			lwPlayer.addTrophiesCollected(trophiesToAdd);
			getPmApi().rewardMoneyTo(lwPlayer, trophiesToAdd * trophyReward);
			getApi().callEvent(new ObjectivePlayerProgressChangeEvent(getApi(), this, lwPlayer));
			checkObjectiveCompleted(lwPlayer);
		}
	}
	
	private void checkObjectiveCompleted(LostWorldPlayer lwPlayer) {
		if(!isCompleted() && ((LostWorldTeam) lwPlayer.getTeam()).getTrophiesCollected() >= getLimit()){
			getManager().completeObjective(this, lwPlayer);
		}
	}

}
