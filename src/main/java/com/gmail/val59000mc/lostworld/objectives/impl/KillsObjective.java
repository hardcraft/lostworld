package com.gmail.val59000mc.lostworld.objectives.impl;

import org.bukkit.event.EventHandler;

import com.gmail.val59000mc.hcgameslib.events.HCPlayerKilledByPlayerEvent;
import com.gmail.val59000mc.lostworld.events.ObjectivePlayerProgressChangeEvent;
import com.gmail.val59000mc.lostworld.objectives.AbstractGameObjective;
import com.gmail.val59000mc.lostworld.objectives.ObjectiveManager;
import com.gmail.val59000mc.lostworld.objectives.ObjectiveType;
import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;
import com.gmail.val59000mc.lostworld.players.LostWorldTeam;

public class KillsObjective extends AbstractGameObjective{
	
	private double killReward;
	
	public KillsObjective(ObjectiveManager manager) {
		super(manager, ObjectiveType.KILLS);
	}

	@Override
	public int getCompletion(LostWorldTeam team) {
		return team.getKills();
	}
	
	@Override
	public int getCompletionPercentage(LostWorldTeam team) {
		return (int) Math.min(
			Math.round(100d * ((double) team.getKills() / (double) getLimit())),
			100
		);
	}

	@Override
	protected void load() {
		super.load();
		this.killReward = getApi().getConfig().getDouble("objectives."+getType().name()+".rewards.per-kill",0);
	}
	
	@EventHandler
	public void onKill(HCPlayerKilledByPlayerEvent e){
		LostWorldPlayer lwPlayer = (LostWorldPlayer) e.getHcKiller();
		if(lwPlayer.hasTeam()){
			getPmApi().rewardMoneyTo(lwPlayer, killReward);
			getApi().callEvent(new ObjectivePlayerProgressChangeEvent(getApi(), this, lwPlayer));
			checkObjectiveCompleted(lwPlayer);
		}
	}
	
	private void checkObjectiveCompleted(LostWorldPlayer lwPlayer) {
		if(!isCompleted() && lwPlayer.getTeam().getKills() >= getLimit()){
			getManager().completeObjective(this, lwPlayer);
		}
	}

}
