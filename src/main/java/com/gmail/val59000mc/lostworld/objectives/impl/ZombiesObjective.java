package com.gmail.val59000mc.lostworld.objectives.impl;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDeathEvent;

import com.gmail.val59000mc.lostworld.events.ObjectivePlayerProgressChangeEvent;
import com.gmail.val59000mc.lostworld.objectives.AbstractGameObjective;
import com.gmail.val59000mc.lostworld.objectives.ObjectiveManager;
import com.gmail.val59000mc.lostworld.objectives.ObjectiveType;
import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;
import com.gmail.val59000mc.lostworld.players.LostWorldTeam;

public class ZombiesObjective extends AbstractGameObjective{
	
	private double zombieKillReward;

	
	public ZombiesObjective(ObjectiveManager manager) {
		super(manager, ObjectiveType.ZOMBIES);
	}

	@Override
	public int getCompletion(LostWorldTeam team) {
		return team.getZombiesKilled();
	}

	@Override
	public int getCompletionPercentage(LostWorldTeam team) {
		return (int) Math.round(100d * ((double) team.getZombiesKilled() / (double) getLimit()));
	}
	
	@Override
	protected void load() {
		super.load();
		this.zombieKillReward = getApi().getConfig().getDouble("objectives."+getType().name()+".rewards.per-zombie-kill",0);
	}
	
	@EventHandler
	public void onZombieKill(EntityDeathEvent e){
		if(e.getEntityType().equals(EntityType.ZOMBIE)){
			Zombie zombie = (Zombie) e.getEntity();
			if(zombie.getKiller() != null){
				LostWorldPlayer lwPlayer = (LostWorldPlayer) getPmApi().getHCPlayer(zombie.getKiller());
				if(lwPlayer != null && lwPlayer.hasTeam() && lwPlayer.isPlaying()){
					lwPlayer.addZombiesKilled();
					getPmApi().rewardMoneyTo(lwPlayer, zombieKillReward);
					getApi().callEvent(new ObjectivePlayerProgressChangeEvent(getApi(), this, lwPlayer));
					checkObjectiveCompleted(lwPlayer);
				}
			}
		}
	}

	private void checkObjectiveCompleted(LostWorldPlayer lwPlayer) {
		if(!isCompleted() && ((LostWorldTeam)lwPlayer.getTeam()).getZombiesKilled() >= getLimit()){
			getManager().completeObjective(this, lwPlayer);
		}
	}

}
