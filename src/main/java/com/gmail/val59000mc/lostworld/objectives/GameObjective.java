package com.gmail.val59000mc.lostworld.objectives;

import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;
import com.gmail.val59000mc.lostworld.players.LostWorldTeam;

public interface GameObjective {

	/**
	 * Check if the objective has been started
	 * @return true is started, else false
	 */
	boolean isStarted();
	
	/**
	 * Check if the objective has been completed
	 * @return true if completed, else false
	 */
	boolean isCompleted();
	
	/**
	 * Points given to the team who complete the objective
	 * @return
	 */
	int getScore();
	
	/**
	 * Number of times the objective has to be incremented to be completed
	 * e.g: for a kills objective of 30
	 * @return a number >= 0
	 */
	int getLimit();
	
	/**
	 * Get objective type
	 * @return ObjectiveType
	 */
	ObjectiveType getType();


	/**
	 * Get completion for a team
	 * @param team
	 * @return an int value between 0 and limit
	 */
	int getCompletion(LostWorldTeam team);


	/**
	 * Get completion percentage for a team
	 * @param team
	 * @return a Double value between 0 and 100
	 */
	int getCompletionPercentage(LostWorldTeam team);
	
	
	/**
	 * The team who completed the objective
	 * @return a team or null if not completed
	 */
	LostWorldTeam getCompletedByTeam();
	
	/**
	 * Reward for team in HC upon completion
	 * @return
	 */
	double getTeamReward();
	
	/**
	 * The player who actually completed the objective
	 * @return a player or null if not completed
	 */
	LostWorldPlayer getCompletedByPlayer();
	
	/**
	 * Reward for player in HC upon completion
	 * @return
	 */
	double getPlayerReward();

}
