package com.gmail.val59000mc.lostworld.objectives;

public enum ObjectiveType {
	CASTLE,
	ZOMBIES,
	KILLS,
	TROPHIES,
	BOB,
	TOWERS
}
