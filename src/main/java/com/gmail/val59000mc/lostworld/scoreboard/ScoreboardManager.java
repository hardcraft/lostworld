package com.gmail.val59000mc.lostworld.scoreboard;

import static com.gmail.val59000mc.lostworld.objectives.ObjectiveType.BOB;
import static com.gmail.val59000mc.lostworld.objectives.ObjectiveType.CASTLE;
import static com.gmail.val59000mc.lostworld.objectives.ObjectiveType.KILLS;
import static com.gmail.val59000mc.lostworld.objectives.ObjectiveType.TOWERS;
import static com.gmail.val59000mc.lostworld.objectives.ObjectiveType.TROPHIES;
import static com.gmail.val59000mc.lostworld.objectives.ObjectiveType.ZOMBIES;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.game.GameState;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.lostworld.common.Constants;
import com.gmail.val59000mc.lostworld.events.ObjectiveCompletedEvent;
import com.gmail.val59000mc.lostworld.events.ObjectivePlayerProgressChangeEvent;
import com.gmail.val59000mc.lostworld.events.ObjectiveTeamProgressChangeEvent;
import com.gmail.val59000mc.lostworld.objectives.GameObjective;
import com.gmail.val59000mc.lostworld.objectives.ObjectiveManager;
import com.gmail.val59000mc.lostworld.objectives.impl.BobObjective;
import com.gmail.val59000mc.lostworld.objectives.impl.CastleObjective;
import com.gmail.val59000mc.lostworld.objectives.impl.KillsObjective;
import com.gmail.val59000mc.lostworld.objectives.impl.TowersObjective;
import com.gmail.val59000mc.lostworld.objectives.impl.TrophiesObjective;
import com.gmail.val59000mc.lostworld.objectives.impl.ZombiesObjective;
import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;
import com.gmail.val59000mc.lostworld.players.LostWorldTeam;
import com.gmail.val59000mc.spigotutils.Time;

public class ScoreboardManager extends HCListener{
	
	private ObjectiveManager objectiveManager;
	private long lastUpdate;
	private List<String> content;

	private LostWorldTeam redTeam;
	private LostWorldTeam greenTeam;

	public ScoreboardManager(HCGameAPI api, ObjectiveManager objectiveManager) {
		this.lastUpdate = 0;
		this.content = new ArrayList<>();
		this.objectiveManager = objectiveManager;
		api.registerListener(this);

		this.redTeam = (LostWorldTeam) getPmApi().getHCTeam(Constants.RED_TEAM);
		this.greenTeam = (LostWorldTeam) getPmApi().getHCTeam(Constants.GREEN_TEAM);
	}

	public List<String> getPlayingScoreboardContent(LostWorldPlayer hcPlayer) {

		Long now = System.currentTimeMillis();
		
		if(now - lastUpdate >= 1000)
			refreshContent();
		
		return content;
		
	}
	
	private void refreshContent(){
		content.clear();
		
		content.add(getStringsApi().get("messages.scoreboard.scores").toString());
		// Team scores
		for(HCTeam team : getPmApi().getTeams()){
			content.add(" "+team.getColoredName()+" "+ChatColor.WHITE+((LostWorldTeam) team).getScore());
		}
		
		// Remaining time (if exists)
		if(getApi().is(GameState.PLAYING) && getApi().isCountdownEndOfGameEnabled()){
			int remainingTime = getApi().getRemainingTimeBeforeEnd();
			content.add(
					getStringsApi().get("messages.scoreboard.remaining-time-before-end").toString()+
					" "+
					ChatColor.GREEN+Time.getFormattedTime(remainingTime)
			);
		}


		content.add(getStringsApi().get("lostworld.scoreboard.objectives").toString());
		
		// Objectives
		CastleObjective castle = (CastleObjective) objectiveManager.getObjective(CASTLE);
		if(!castle.isCompleted()){
			content.add(getObjectiveScoreboardName(castle));
			content.add(getTeamsCompletionsPercentage(redTeam, greenTeam, castle));
			
		}else{
			ZombiesObjective zombies = (ZombiesObjective) objectiveManager.getObjective(ZOMBIES);
			content.add(getObjectiveScoreboardName(zombies));
			content.add(getTeamsCompletionsPercentage(redTeam, greenTeam, zombies));
			
			KillsObjective kills = (KillsObjective) objectiveManager.getObjective(KILLS);
			content.add(getObjectiveScoreboardName(kills));
			content.add(getTeamsCompletionsPercentage(redTeam, greenTeam, kills));
			
			TrophiesObjective trophies = (TrophiesObjective) objectiveManager.getObjective(TROPHIES);
			content.add(getObjectiveScoreboardName(trophies));
			content.add(getTeamsCompletionsPercentage(redTeam, greenTeam, trophies));
			
			BobObjective bob = (BobObjective) objectiveManager.getObjective(BOB);
			content.add(getObjectiveScoreboardName(bob));
			content.add(getTeamsCompletionsPercentage(redTeam, greenTeam, bob));
			
			TowersObjective towers = (TowersObjective) objectiveManager.getObjective(TOWERS);
			content.add(getObjectiveScoreboardName(towers));
			content.add(getTeamsCompletionsPercentage(redTeam, greenTeam, towers));
			
		}
		
		lastUpdate = System.currentTimeMillis();
	}

	private String getObjectiveScoreboardName(GameObjective objective){
		return getStringsApi()
				.get("lostworld.scoreboard."+objective.getType().name())
				.replace("%limit%", String.valueOf(objective.getLimit()))
				.replace("%iscompleted%",objective.isCompleted() ? ChatColor.STRIKETHROUGH.toString() : "")
				.toString();
	}
	
	private String getTeamsCompletionsPercentage(LostWorldTeam redTeam, LostWorldTeam greenTeam, GameObjective objective) {
		
		StringBuilder progress = new StringBuilder();
		
		// red
		int redCompletion = (int) Math.ceil(objective.getCompletionPercentage(redTeam)) / 10;
		if(redCompletion == 0){
			progress.append(ChatColor.GRAY);
			progress.append("||||||||||");
		}else if(redCompletion == 10){
			progress.append(redTeam.getColor());
			progress.append("||||||||||");
		}else{
			progress.append(redTeam.getColor());
			for(int i=1 ; i<=redCompletion ; i++){
				progress.append("|");
			}
			progress.append(ChatColor.GRAY);
			for(int i=redCompletion+1 ; i<=10 ; i++){
				progress.append("|");
			}
		}
		if(redCompletion != 10){
			progress.append(redTeam.getColor());
		}
		progress.append(Constants.RIGHT_ARROW_ICON);
		
		// center
		if(objective.isCompleted()){
			if(objective.getCompletedByTeam().equals(greenTeam)){
				progress.append(greenTeam.getColor());
			}
		}else{
			progress.append(ChatColor.WHITE);
		}
		progress.append(Constants.SCOREBOARD_SWORD_ICON);
		
		// green
		int greenCompletion = (int) Math.ceil(objective.getCompletionPercentage(greenTeam)) / 10;
		if(greenCompletion != 10){
			progress.append(greenTeam.getColor());
		}
		progress.append(Constants.LEFT_ARROW_ICON);
		if(greenCompletion == 0){
			progress.append(ChatColor.GRAY);
			progress.append("||||||||||");
		}else if(greenCompletion == 10){
			progress.append("||||||||||");
		}else{
			progress.append(ChatColor.GRAY);
			for(int i=10 ; i>=greenCompletion+1 ; i--){
				progress.append("|");
			}
			progress.append(greenTeam.getColor());
			for(int i=greenCompletion ; i>=1 ; i--){
				progress.append("|");
			}
		}
		
		// unique key
		progress.append(ChatColor.COLOR_CHAR);
		progress.append(String.valueOf(objective.getType().ordinal()));
		
		return progress.toString();
	}
	
	////////////
	// Listener
	////////////
	@EventHandler
	public void onObjectiveComplete(ObjectiveCompletedEvent e){
		refreshContent();
		getPmApi().updatePlayersScoreboards();
	}
	
	@EventHandler
	public void onObjectiveIncrease(ObjectivePlayerProgressChangeEvent e){
		refreshContent();
		getPmApi().updatePlayersScoreboards();
	}
	
	@EventHandler
	public void onObjectiveIncrease(ObjectiveTeamProgressChangeEvent e){
		refreshContent();
		getPmApi().updatePlayersScoreboards();
	}

}
