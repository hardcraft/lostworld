package com.gmail.val59000mc.lostworld.common;

import org.bukkit.ChatColor;

public class Constants {

	public final static String GREEN_TEAM = "Vert";
	public final static String RED_TEAM = "Rouge";
	
	public final static String INITIAL_RED_STUFF = "INITIAL_RED_STUFF";
	public final static String INITIAL_GREEN_STUFF = "INITIAL_GREEN_STUFF";
	public final static String RED_STUFF = "RED_STUFF";
	public final static String GREEN_STUFF = "GREEN_STUFF";
	public static final String SCOREBOARD_SWORD_ICON = "\u2694";
	public static final String RIGHT_ARROW_ICON = "\u25BA";
	public static final String LEFT_ARROW_ICON = "\u25C4";
	
	public static final String BOB_INTERNAL_NAME = "Bob";
	public static final String BOB_DISPLAY_NAME = ChatColor.YELLOW+"°°°BoB°°°";
	public static final double BOB_KILL_REWARD = 10;

	public static final String FLYING_SKELETON_INTERNAL_NAME = "FlyingSkeleton";
	
	public static final String ALL_ITEM_STORE = "all";
	public static final int MIN_CHEST_ITEMS = 4;
	public static final int MAX_CHEST_ITEMS = 12;
	public static final int CUSTOM_ENCHANT_CHANCE = 35;
	public static final int CUSTOM_POTION_CHANCE = 45;
}
