package com.gmail.val59000mc.lostworld;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.game.HCGame;
import com.gmail.val59000mc.lostworld.callbacks.LostWorldCallbacks;
import com.gmail.val59000mc.lostworld.commands.LostWorldTestCommand;
import com.gmail.val59000mc.lostworld.listeners.*;
import com.gmail.val59000mc.spigotutils.Configurations;
import com.google.common.collect.Sets;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class LostWorld extends JavaPlugin {

    public void onEnable() {

        this.getDataFolder().mkdirs();
        File config = Configurations.saveConfigDefaultsToFile(Configurations.getConfigFromResource(this, "config.yml"), new File(this.getDataFolder(), "config.yml"));
        File lang = Configurations.saveConfigDefaultsToFile(Configurations.getConfigFromResource(this, "lang.yml"), new File(this.getDataFolder(), "lang.yml"));

        HCGameAPI game = new HCGame.Builder("Lost World", this, config, lang)
            .withPluginCallbacks(new LostWorldCallbacks())
            .withDefaultListenersAnd(Sets.newHashSet(
                new LostWorldBlockListener(),
                new LostWorldDamageListener(),
                new ButtonsListener(),
                new ChestsListener(),
                new ObjectiveListener(),
                new NoPvpBeforeCastleListener(),
                new SimpleInventoryGUIListener(),
                new FlyingSkeletonListener(),
                new CastlesListener(),
                new ItemSpawnerListener(),
                new BellsListener()
                ))
            .withDefaultCommandsAnd(Sets.newHashSet(
                new LostWorldTestCommand(this)
            ))
            .build();
        game.loadGame();
    }

    public void onDisable() {
    }
}
