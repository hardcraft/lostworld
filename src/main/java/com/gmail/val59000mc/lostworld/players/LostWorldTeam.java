package com.gmail.val59000mc.lostworld.players;

import org.bukkit.ChatColor;
import org.bukkit.Location;

import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.stuff.Stuff;

public class LostWorldTeam extends HCTeam{

	private Location castleSpawnpoint;
	private Stuff initialStuff;
	private Stuff stuff;
	
	private int score;
	
	private int trophiesCollected;
	private int zombiesKilled;
	private int obsidiansBroken;
	private boolean hasTakenBob;
	private boolean hasReachedCastle;
	
	public LostWorldTeam(String name, ChatColor color, Location initialSpawnpoint, Location castleSpawnpoint){
		super(name, color, initialSpawnpoint);
		this.castleSpawnpoint = castleSpawnpoint;
		this.initialStuff = null;
		this.stuff = null;
		this.score = 0;
		this.trophiesCollected = 0;
		this.zombiesKilled = 0;
		this.obsidiansBroken = 0;
		this.hasTakenBob = false;
		this.hasReachedCastle = false;
	}
	
	public void moveToCastleSpawnpoint(){
		this.spawnpoint = castleSpawnpoint;
		for(HCPlayer hcPlayer : getMembers(null, PlayerState.PLAYING)){
			hcPlayer.setSpawnPoint(spawnpoint);
		}
	}
	public Location getCastleSpawnpoint() {
		return castleSpawnpoint;
	}

	public Stuff getInitialStuff() {
		return initialStuff;
	}

	public void setInitialStuff(Stuff initialStuff) {
		this.initialStuff = initialStuff;
	}

	public Stuff getStuff() {
		return stuff;
	}

	public void setStuff(Stuff stuff) {
		this.stuff = stuff;
	}

	public int getScore() {
		return score;
	}

	public void addScore(int amount) {
		this.score += amount;
	}

	public int getTrophiesCollected() {
		return trophiesCollected;
	}

	public void addTrophiesCollected(int amount) {
		this.trophiesCollected += amount;
	}

	public int getZombiesKilled() {
		return zombiesKilled;
	}

	public void addZombiesKilled() {
		this.zombiesKilled++;
	}

	public int getObsidiansBroken() {
		return obsidiansBroken;
	}

	public void addObsidiansBroken() {
		this.obsidiansBroken++;
	}

	public boolean hasTakenBob() {
		return hasTakenBob;
	}

	public void setTakenBob() {
		this.hasTakenBob = true;
	}

	public boolean hasReachedCastle() {
		return hasReachedCastle;
	}

	public void setReachedCastle() {
		this.hasReachedCastle = true;
	}

	

	

}
