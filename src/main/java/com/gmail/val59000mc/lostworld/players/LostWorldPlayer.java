package com.gmail.val59000mc.lostworld.players;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

public class LostWorldPlayer extends HCPlayer{

	private int score;
	
	private int trophiesCollected;
	private int zombiesKilled;
	private int obsidiansBroken;
	private boolean hasTakenBob;
	private boolean hasReachedCastle;
	
	public LostWorldPlayer(Player player) {
		super(player);
		this.score = 0;
		this.trophiesCollected = 0;
		this.zombiesKilled = 0;
		this.obsidiansBroken = 0;
		this.hasTakenBob = false;
		this.hasReachedCastle = false;
	}
	
	public LostWorldPlayer(HCPlayer hcPlayer){
		super(hcPlayer);
		LostWorldPlayer lostWorldPlayer = (LostWorldPlayer) hcPlayer;
		this.score = lostWorldPlayer.score;
		this.trophiesCollected = lostWorldPlayer.trophiesCollected;
		this.zombiesKilled = lostWorldPlayer.zombiesKilled;
		this.obsidiansBroken = lostWorldPlayer.obsidiansBroken;
		this.hasTakenBob = lostWorldPlayer.hasTakenBob;
		this.hasReachedCastle = lostWorldPlayer.hasReachedCastle;
	}
	
	public void subtractBy(HCPlayer lastUpdatedSession) {
		super.subtractBy(lastUpdatedSession);
		LostWorldPlayer lostWorldPlayer = (LostWorldPlayer) lastUpdatedSession;
		this.score -= lostWorldPlayer.score;
		this.trophiesCollected -= lostWorldPlayer.trophiesCollected;
		this.zombiesKilled -= lostWorldPlayer.zombiesKilled;
		this.obsidiansBroken -= lostWorldPlayer.obsidiansBroken;
		this.hasTakenBob = this.hasTakenBob ^ lostWorldPlayer.hasTakenBob;
		this.hasReachedCastle = this.hasTakenBob ^ lostWorldPlayer.hasReachedCastle;
	}

	public int getScore() {
		return score;
	}

	public void addScore(int amount) {
		this.score += amount;
		if(hasTeam()){
			((LostWorldTeam)getTeam()).addScore(amount);
		}
	}

	public int getTrophiesCollected() {
		return trophiesCollected;
	}

	public void addTrophiesCollected(int amount) {
		this.trophiesCollected += amount;
		if(hasTeam()){
			((LostWorldTeam) getTeam()).addTrophiesCollected(amount);
		}
	}

	public int getZombiesKilled() {
		return zombiesKilled;
	}

	public void addZombiesKilled() {
		this.zombiesKilled++;
		if(hasTeam()){
			((LostWorldTeam) getTeam()).addZombiesKilled();
		}
	}

	public int getObsidiansBroken() {
		return obsidiansBroken;
	}

	public void addObsidiansBroken() {
		this.obsidiansBroken ++;
		if(hasTeam()){
			((LostWorldTeam) getTeam()).addObsidiansBroken();
		}
	}

	public boolean hasTakenBob() {
		return hasTakenBob;
	}

	public void setTakenBob() {
		this.hasTakenBob = true;
		if(hasTeam()){
			((LostWorldTeam)getTeam()).setTakenBob();
		}
	}

	public boolean hasReachedCastle() {
		return hasReachedCastle;
	}

	public void setReachedCastle() {
		this.hasReachedCastle = true;
		if(hasTeam()){
			((LostWorldTeam)getTeam()).setReachedCastle();
		}
	}
	
	
	

}
