package com.gmail.val59000mc.lostworld.buttons;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;
import com.gmail.val59000mc.spigotutils.Colors;
import com.gmail.val59000mc.spigotutils.Randoms;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;

import java.util.List;

public class RandomTeleportButtonAction extends AbstractButtonAction {

    private List<Location> destinations;

    public RandomTeleportButtonAction(HCGameAPI api, Location location, List<Location> destinations) {
        super(api, location);
        this.destinations = destinations;
    }

    @Override
    public void apply(LostWorldPlayer lwPlayer) {
        if (lwPlayer.isPlaying()) {
            Location randomLocation = destinations.get(Randoms.randomInteger(0, destinations.size() - 1));
            lwPlayer.getPlayer().teleport(randomLocation);
            getAPI().getSoundAPI().play(lwPlayer, Sound.ENTITY_ENDERMAN_TELEPORT, 0.5f, 2);
            getAPI().getStringsAPI().get("lostworld.buttons.random-teleportation.applied")
                .sendChatP(lwPlayer);
            spawnFirework(randomLocation, Colors.toColor(lwPlayer.getColor()));
        }
    }

    private void spawnFirework(Location location, Color color) {
        Firework fw = (Firework) location.getWorld().spawnEntity(location, EntityType.FIREWORK);
        FireworkMeta fwm = fw.getFireworkMeta();

        Color secondaryColor = Color.fromRGB(255, 255, 255);

        //Create our effect with this
        FireworkEffect effect = FireworkEffect.builder()
            .with(Type.BALL_LARGE)
            .flicker(true)
            .withColor(color, secondaryColor)
            .withFade(color, secondaryColor)
            .build();

        //Then apply the effect to the meta
        fwm.addEffect(effect);

        //Generate some random power and set it
        fwm.setPower(1);

        //Then apply this to our rocket
        fw.setFireworkMeta(fwm);

        getAPI().sync(() -> fw.detonate(), 5);
    }

}
