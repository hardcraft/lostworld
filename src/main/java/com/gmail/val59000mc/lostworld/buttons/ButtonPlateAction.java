package com.gmail.val59000mc.lostworld.buttons;

import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;

public interface ButtonPlateAction {

	void apply(LostWorldPlayer lwPlayer);
}
