package com.gmail.val59000mc.lostworld.buttons;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.lostworld.callbacks.LostWorldCallbacks;
import com.gmail.val59000mc.lostworld.objectives.ObjectiveType;
import com.gmail.val59000mc.lostworld.objectives.impl.CastleObjective;
import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;

public class JumpPlateAction extends AbstractButtonAction{
	
	private int jumpLevel;
	
	public JumpPlateAction(HCGameAPI api, Location location, int jumpLevel) {
		super(api, location);
		this.jumpLevel = jumpLevel;
	}

	@Override
	public void apply(LostWorldPlayer lwPlayer) {		
		if(lwPlayer.isPlaying()){
			CastleObjective castle = (CastleObjective) ((LostWorldCallbacks) getAPI().getCallbacksApi()).getObjectiveManager().getObjective(ObjectiveType.CASTLE);
			if(castle.isCompleted()){
				Player player = lwPlayer.getPlayer();
				getAPI().getSoundAPI().play(Sound.ENTITY_FIREWORK_ROCKET_LAUNCH, player.getLocation(), 1, 2);
				player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 50, jumpLevel), true);
				player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 140, 255), true);
				getAPI().getStringsAPI().get("lostworld.buttons.plates.jump").sendChat(lwPlayer);
			}else{
				getAPI().getStringsAPI().get("lostworld.buttons.plates.cannot-use-now").sendChat(lwPlayer);
			}
		}
	}

}
