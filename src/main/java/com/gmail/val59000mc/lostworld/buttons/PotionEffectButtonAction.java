package com.gmail.val59000mc.lostworld.buttons;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.potion.PotionEffect;

public class PotionEffectButtonAction extends AbstractButtonAction {

    private PotionEffect effect;

    public PotionEffectButtonAction(HCGameAPI api, Location location, PotionEffect effect) {
        super(api, location);
        this.effect = effect;
    }

    @Override
    public void apply(LostWorldPlayer lwPlayer) {
        if (lwPlayer.isPlaying()) {
            lwPlayer.getPlayer().addPotionEffect(effect, true);
            getAPI().getSoundAPI().play(lwPlayer, Sound.ENTITY_CHICKEN_EGG, 0.5f, 2);
            getAPI().getStringsAPI().get("lostworld.buttons.effects.applied")
                .replace("%effect%", getAPI().getStringsAPI().get("lostworld.buttons.effects." + effect.getType().getName()).toString())
                .replace("%amplifier%", String.valueOf(1 + effect.getAmplifier()))
                .replace("%duration%", String.valueOf((int) (0.05d * (double) effect.getDuration())))
                .sendChat(lwPlayer);
        }
    }

}
