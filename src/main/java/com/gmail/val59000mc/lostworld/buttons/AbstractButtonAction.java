package com.gmail.val59000mc.lostworld.buttons;

import org.bukkit.Location;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;

public abstract class AbstractButtonAction implements ButtonPlateAction{

	private HCGameAPI api;
	
	/**
	 * Physical button location in the world
	 */
	private Location location;
	
	public AbstractButtonAction(HCGameAPI api, Location location){
		this.api = api;
		this.location = location;
	}
	
	protected HCGameAPI getAPI(){
		return api;
	}

	public Location getLocation() {
		return location;
	}
	
}
