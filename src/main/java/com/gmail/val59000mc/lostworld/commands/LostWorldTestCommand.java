package com.gmail.val59000mc.lostworld.commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

import com.gmail.val59000mc.hcgameslib.commands.HCCommand;
import com.gmail.val59000mc.lostworld.callbacks.LostWorldCallbacks;
import com.gmail.val59000mc.lostworld.events.BobSpawnEvent;
import com.gmail.val59000mc.lostworld.events.FillChestEvent;
import com.gmail.val59000mc.lostworld.items.ChestsManager;
import com.gmail.val59000mc.lostworld.items.LostWorldtems;
import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;
import com.gmail.val59000mc.lostworld.players.LostWorldTeam;

public class LostWorldTestCommand extends HCCommand{

	
	public LostWorldTestCommand(JavaPlugin plugin) {
		super(plugin, "test");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player){
			
			Player player = (Player) sender;
			
			if(args.length == 0){
				player.sendMessage("§cMissing argument !");
			}else{
				switch(args[0]){
					case "castle":
						tpCastle(player);
						break;
					case "center":
						tpCenter(player);
						break;
					case "chest":
						spawnFilledChest(player, args);
						break;
					case "chest-event":
						spawnFilledChestEvent();
						break;
					case "trophy":
						player.getInventory().addItem(LostWorldtems.getTrophyItem(5));
						break;
					case "bob":
						player.getInventory().addItem(LostWorldtems.getBobHead());
						break;
					case "bob-event":
						spawnBobEvent();
						break;
					case "velocity":
						testVelocity(player, args);
						break;
					default:
						player.sendMessage("§cUnknown command !");
						break;
				}
			}
		}
		return true;
	}

	private void testVelocity(Player player, String[] args) {
		if(args.length >= 2){
			try{
				double y = Double.parseDouble(args[1]);
				player.setVelocity(new Vector(0,y,0));
			}catch(NumberFormatException e){
				player.sendMessage("Second argument must be a number, /test velocity <decimalNumber>");
			}
		}
		
	}

	private void spawnFilledChest(Player player, String[] args) {
		Vector playerDirection = player.getLocation().getDirection().normalize();
		playerDirection.setY(0);
		Location frontOfPlayer = player.getLocation().add(playerDirection);
		
		ChestsManager chests = ((LostWorldCallbacks) getCallbacksApi()).getChestsManager();
		
		Integer nbItems = 10;
		
		if(args.length == 2){
			try{
				nbItems = Integer.parseInt(args[1]);
			}catch(NumberFormatException e){
				player.sendMessage("Second optional argument must be a number, /test chest [maxNumberOfItems]");
				return;
			}
		}
			
		((LostWorldCallbacks) getCallbacksApi()).getChestsManager().placeChestInWorld(frontOfPlayer, chests.getRandomItems(nbItems));
		
	}


	private void spawnFilledChestEvent() {
		getAPi().callEvent(new FillChestEvent(getAPi()));		
	}


	private void spawnBobEvent() {
		getAPi().callEvent(new BobSpawnEvent(getAPi()));		
	}


	private void tpCastle(Player player) {
		LostWorldPlayer lwPlayer = (LostWorldPlayer) getPmApi().getHCPlayer(player);
		if(lwPlayer.isPlaying()){
			LostWorldTeam team = (LostWorldTeam) lwPlayer.getTeam();
			player.teleport(team.getCastleSpawnpoint());			
		}
		
	}

	private void tpCenter(Player player) {
		LostWorldPlayer lwPlayer = (LostWorldPlayer) getPmApi().getHCPlayer(player);
		if(lwPlayer.isPlaying()){
			player.teleport(getAPi().getWorldConfig().getCenter());			
		}
		
	}

}
