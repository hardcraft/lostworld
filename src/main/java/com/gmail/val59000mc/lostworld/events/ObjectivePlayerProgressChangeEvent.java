package com.gmail.val59000mc.lostworld.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;
import com.gmail.val59000mc.lostworld.objectives.GameObjective;
import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;

public class ObjectivePlayerProgressChangeEvent extends HCEvent{

	private GameObjective objective;
	
	private LostWorldPlayer lwPlayer;
	
	public ObjectivePlayerProgressChangeEvent(HCGameAPI api, GameObjective objective, LostWorldPlayer lwPlayer) {
		super(api);
		this.objective = objective;
		this.lwPlayer = lwPlayer;
	}

	public GameObjective getObjective() {
		return objective;
	}

	public LostWorldPlayer getLwPlayer() {
		return lwPlayer;
	}
	
}
