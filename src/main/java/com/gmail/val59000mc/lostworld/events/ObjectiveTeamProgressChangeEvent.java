package com.gmail.val59000mc.lostworld.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;
import com.gmail.val59000mc.lostworld.objectives.GameObjective;
import com.gmail.val59000mc.lostworld.players.LostWorldTeam;

public class ObjectiveTeamProgressChangeEvent extends HCEvent{

	private GameObjective objective;
	
	private LostWorldTeam lwTeam;
	
	public ObjectiveTeamProgressChangeEvent(HCGameAPI api, GameObjective objective, LostWorldTeam lwTeam) {
		super(api);
		this.objective = objective;
		this.lwTeam = lwTeam;
	}

	public GameObjective getObjective() {
		return objective;
	}

	public LostWorldTeam getLwTeam() {
		return lwTeam;
	}
	
}
