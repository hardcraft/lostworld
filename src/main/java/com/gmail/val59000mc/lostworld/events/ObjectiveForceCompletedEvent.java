package com.gmail.val59000mc.lostworld.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;
import com.gmail.val59000mc.lostworld.objectives.GameObjective;

public class ObjectiveForceCompletedEvent extends HCEvent{

	private GameObjective objective;
	
	public ObjectiveForceCompletedEvent(HCGameAPI api, GameObjective objective) {
		super(api);
		this.objective = objective;
	}

	public GameObjective getObjective() {
		return objective;
	}

}
