package com.gmail.val59000mc.lostworld.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;
import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;

public class TrophyCollectedEvent extends HCEvent{

	private LostWorldPlayer lwPlayer;
	
	private int amount;
	
	public TrophyCollectedEvent(HCGameAPI api, LostWorldPlayer lwPlayer, int amount){
		super(api);
		this.lwPlayer = lwPlayer;
		this.amount = amount;
	}

	public LostWorldPlayer getLostWorldPlayer() {
		return lwPlayer;
	}

	public int getAmount() {
		return amount;
	}
	
	
}
