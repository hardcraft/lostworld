package com.gmail.val59000mc.lostworld.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;
import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;

public class BobCollectedEvent extends HCEvent{

	private LostWorldPlayer lwPlayer;
		
	public BobCollectedEvent(HCGameAPI api, LostWorldPlayer lwPlayer){
		super(api);
		this.lwPlayer = lwPlayer;
	}

	public LostWorldPlayer getLostWorldPlayer() {
		return lwPlayer;
	}
	
	
}
