package com.gmail.val59000mc.lostworld.listeners;

import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.lostworld.events.ObjectiveCompletedEvent;
import com.gmail.val59000mc.lostworld.objectives.ObjectiveType;
import com.gmail.val59000mc.spigotutils.Locations;
import com.gmail.val59000mc.spigotutils.Randoms;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.type.Bell;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

import static org.bukkit.block.BlockFace.*;

public class BellsListener extends HCListener {

    private boolean isRingingBellDropsXp;

    /**
     * Enabling bell xp only after castle objective has been completed
     */
    @EventHandler(ignoreCancelled = true)
    public void onRingBell(ObjectiveCompletedEvent e) {
        if (ObjectiveType.CASTLE.equals(e.getObjective().getType())) {
            isRingingBellDropsXp = true;
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onRingBell(PlayerInteractEvent e) {
        if (isRingingBell(e)) {

            Block bellBlock = e.getClickedBlock();
            ExperienceOrb xpOrb = (ExperienceOrb) bellBlock.getWorld()
                .spawnEntity(Locations.toCenterBlock(bellBlock.getLocation()).subtract(0, 0.5, 0), EntityType.EXPERIENCE_ORB);
            xpOrb.setExperience(3);

            Vector vector = new Vector(
                Randoms.randomDouble(0d, 1d) - 0.5d,
                -0.1d,
                Randoms.randomDouble(0d, 1d) - 0.5d
            );
            xpOrb.setVelocity(vector);

        }
    }

    private boolean isRingingBell(PlayerInteractEvent e) {
        if (!isRingingBellDropsXp) {
            return false;
        }

        Block bellBlock = e.getClickedBlock();
        BlockFace clickedFace = e.getBlockFace();
        if (e.getAction() != Action.RIGHT_CLICK_BLOCK
            || bellBlock == null
            || !Material.BELL.equals(bellBlock.getType())
        ) {
            return false;
        }

        HCPlayer hcPlayer = getPmApi().getHCPlayer(e.getPlayer());
        if (hcPlayer == null || !hcPlayer.isPlaying()) {
            return false;
        }

        Bell bellData = (Bell) bellBlock.getBlockData();
        BlockFace bellFacing = bellData.getFacing();
        boolean isRingingBell = false;
        switch (bellData.getAttachment()) {
            case FLOOR:
                // hit by parallel side
                switch (bellFacing) {
                    case NORTH:
                    case SOUTH:
                        isRingingBell = clickedFace.equals(NORTH) || clickedFace.equals(SOUTH);
                        break;
                    case EAST:
                    case WEST:
                        isRingingBell = clickedFace.equals(EAST) || clickedFace.equals(WEST);
                        break;
                }
                break;
            case CEILING:
                // hit by any side
                isRingingBell = true;
                break;
            case SINGLE_WALL:
            case DOUBLE_WALL:
                // hit by perpendicular side
                switch (bellFacing) {
                    case NORTH:
                    case SOUTH:
                        isRingingBell = clickedFace.equals(EAST) || clickedFace.equals(WEST);
                        break;
                    case EAST:
                    case WEST:
                        isRingingBell = clickedFace.equals(NORTH) || clickedFace.equals(SOUTH);
                        break;
                }
                break;
        }

        return isRingingBell;
    }

}
