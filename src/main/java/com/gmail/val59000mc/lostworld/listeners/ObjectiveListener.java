package com.gmail.val59000mc.lostworld.listeners;

import com.gmail.val59000mc.hcgameslib.api.impl.HCMessage;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.lostworld.callbacks.LostWorldCallbacks;
import com.gmail.val59000mc.lostworld.events.*;
import com.gmail.val59000mc.lostworld.items.LostWorldtems;
import com.gmail.val59000mc.lostworld.objectives.GameObjective;
import com.gmail.val59000mc.lostworld.objectives.ObjectiveManager;
import com.gmail.val59000mc.lostworld.objectives.ObjectiveType;
import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;
import com.gmail.val59000mc.lostworld.players.LostWorldTeam;
import com.gmail.val59000mc.lostworld.scoreboard.ScoreboardManager;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public class ObjectiveListener extends HCListener {

    private ObjectiveManager objectiveManager;
    private ScoreboardManager scoreboardManager;

    private Map<LostWorldTeam, Map<ObjectiveType, Integer>> milestonesProgress;

    @EventHandler(priority = EventPriority.NORMAL)
    public void afterLoad(HCAfterLoadEvent e) {
        this.objectiveManager = new ObjectiveManager(getApi());
        this.scoreboardManager = new ScoreboardManager(getApi(), objectiveManager);
        ((LostWorldCallbacks) getCallbacksApi()).setObjectiveManager(objectiveManager);
        ((LostWorldCallbacks) getCallbacksApi()).setScoreboardManager(scoreboardManager);
        LostWorldtems.setup(getApi());

        this.milestonesProgress = new HashMap<>();

        load();
    }


    private void load() {

        // initialize milestonesProgress
        for (HCTeam team : getPmApi().getTeams()) {
            Map<ObjectiveType, Integer> progress = new EnumMap<>(ObjectiveType.class);
            for (ObjectiveType type : ObjectiveType.values()) {
                progress.put(type, 0);
            }
            milestonesProgress.put((LostWorldTeam) team, progress);
        }
    }


    // messages and sounds when an objective is completed
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onObjectiveCompleted(ObjectiveCompletedEvent e) {

        GameObjective obj = e.getObjective();

        getStringsApi().get("lostworld.objectives.completed")
            .replace("%objective%",
                getStringsApi().getNoColor("lostworld.objectives." + obj.getType().name() + ".name")
                    .replace("%limit%", String.valueOf(obj.getLimit()))
                    .toString()
            )
            .replace("%player%", obj.getCompletedByPlayer().getColoredName())
            .sendChatP();

        getStringsApi().get("lostworld.objectives.scoring")
            .replace("%team%", obj.getCompletedByTeam().getColoredName())
            .replace("%score%", String.valueOf(obj.getScore()))
            .replace("%pluralScore%", obj.getScore() >= 2 ? "s" : "")
            .sendChatP();

        getSoundApi().play(Sound.ENTITY_ENDER_DRAGON_GROWL, 1, 2);

        getStringsApi().get("lostworld.objectives.completed-title")
            .replace("%team%", obj.getCompletedByTeam().getColoredName())
            .replace("%score%", String.valueOf(obj.getScore()))
            .replace("%objective%", ChatColor.GRAY +
                getStringsApi().getNoColor("lostworld.objectives." + obj.getType().name() + ".name")
                    .replace("%limit%", String.valueOf(obj.getLimit()))
                    .toString()
            )
            .sendTitle(20, 60, 20);
    }

    // messages and sounds when an objective is completed
    @EventHandler
    public void onObjectiveForceCompleted(ObjectiveForceCompletedEvent e) {

        GameObjective obj = e.getObjective();

        getStringsApi().get("lostworld.objectives.force-completed")
            .replace("%objective%",
                getStringsApi().getNoColor("lostworld.objectives." + obj.getType().name() + ".name")
                    .replace("%limit%", String.valueOf(obj.getLimit()))
                    .toString()
            )
            .replace("%team%", obj.getCompletedByTeam().getColoredName())
            .sendChatP();

        getStringsApi().get("lostworld.objectives.scoring")
            .replace("%team%", obj.getCompletedByTeam().getColoredName())
            .replace("%score%", String.valueOf(obj.getScore()))
            .replace("%pluralScore%", obj.getScore() >= 2 ? "s" : "")
            .sendChatP();

        getSoundApi().play(Sound.BLOCK_NOTE_BLOCK_HARP, 1, 2);

    }


    @EventHandler
    public void onObjectiveProgressChange(ObjectivePlayerProgressChangeEvent e) {
        GameObjective obj = e.getObjective();
        LostWorldPlayer lwPlayer = e.getLwPlayer();
        LostWorldTeam lwTeam = (LostWorldTeam) lwPlayer.getTeam();
        sendObjectiveChangeMessage(obj, lwPlayer, lwTeam);
        alertAllWhenReachMilestone(obj, lwTeam);
    }

    @EventHandler
    public void onObjectiveProgressChange(ObjectiveTeamProgressChangeEvent e) {
        alertAllWhenReachMilestone(e.getObjective(), e.getLwTeam());
    }

    // reset last bob progress when item droped
    @EventHandler
    public void onBobPickedUpEvent(BobPickedUpEvent e) {
        getStringsApi()
            .get("lostworld.objectives.BOB.head-picked-up")
            .replace("%player%", e.getLostWorldPlayer().getColoredName())
            .sendChatP();
        getSoundApi().play(Sound.ENTITY_CHICKEN_EGG, 0.5f, 2);
    }


    // reset last bob progress when item droped
    @EventHandler
    public void onBobDropedEvent(BobDropedEvent e) {
        getStringsApi()
            .get("lostworld.objectives.BOB.head-droped")
            .replace("%player%", e.getLwPlayer().getColoredName())
            .sendChatP();
        getSoundApi().play(Sound.ENTITY_CHICKEN_EGG, 0.5f, 2);

        for (HCTeam hcTeam : getPmApi().getTeams()) {
            milestonesProgress.get(hcTeam).put(ObjectiveType.BOB, 0);
        }
    }

    private void alertAllWhenReachMilestone(GameObjective obj, LostWorldTeam lwTeam) {

        Integer lastProgress = milestonesProgress.get(lwTeam).get(obj.getType());
        Integer newProgress = obj.getCompletionPercentage(lwTeam);

        if (newProgress > lastProgress) {
            milestonesProgress.get(lwTeam).put(obj.getType(), newProgress);
        } else {
            return;
        }

        if ((newProgress >= 75 && lastProgress < 75)
            || (newProgress >= 50 && lastProgress < 50)
            || (newProgress >= 25 && lastProgress < 25)) {

            HCMessage msg = getStringsApi().get("lostworld.objectives.progress-broadcast")
                .replace("%team%", lwTeam.getColoredName())
                .replace("%progress%", ratioToColor(newProgress) + String.valueOf(newProgress))
                .replace("%objective%",
                    getStringsApi().getNoColor("lostworld.objectives." + obj.getType().name() + ".name")
                        .replace("%limit%", String.valueOf(obj.getLimit()))
                        .toString()
                );

            if (obj.getLimit() > 1) {
                ChatColor color = ratioToColor(newProgress);

                msg = msg.append(getStringsApi().get("lostworld.objectives.progress-count")
                    .replace("%count%", color + String.valueOf(obj.getCompletion(lwTeam)))
                    .replace("%limit%", String.valueOf(obj.getLimit()))
                    .toString());

            }

            msg.sendChatP();

            getSoundApi().play(Sound.BLOCK_NOTE_BLOCK_HARP, 1, 2);

        }


    }


    private void sendObjectiveChangeMessage(GameObjective obj, LostWorldPlayer lwPlayer, LostWorldTeam lwTeam) {


        HCMessage msg = getStringsApi()
            .get("lostworld.objectives." + obj.getType().name() + ".name")
            .replace("%limit%", String.valueOf(obj.getLimit()));

        int limit = obj.getLimit();

        if (limit > 1) {
            int count = obj.getCompletion(lwTeam);
            Integer ratio = (int) Math.round(100 * (double) count / (double) limit);
            ChatColor color = ratioToColor(ratio);

            msg = msg.append(getStringsApi().get("lostworld.objectives.progress-count")
                .replace("%count%", color + String.valueOf(count))
                .replace("%limit%", String.valueOf(limit))
                .toString());

        }

        msg.sendActionBar(lwTeam);
        msg.sendChat(lwPlayer);

    }

    /**
     * Convert a value between 0 and 100 to a color from red to green
     * 0-25 green
     * 25-50 yellow
     * 50-75 gold
     * 75-100 red
     *
     * @param ratio
     * @return
     */
    private ChatColor ratioToColor(Integer ratio) {
        ChatColor color;
        if (ratio >= 75)
            color = ChatColor.RED;
        else if (ratio >= 50)
            color = ChatColor.GOLD;
        else if (ratio >= 25)
            color = ChatColor.YELLOW;
        else
            color = ChatColor.GREEN;
        return color;
    }
}
