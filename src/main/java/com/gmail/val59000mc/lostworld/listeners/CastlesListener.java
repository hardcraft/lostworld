package com.gmail.val59000mc.lostworld.listeners;

import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.lostworld.castles.CastleBounds;
import com.gmail.val59000mc.lostworld.common.Constants;
import com.gmail.val59000mc.spigotutils.Locations;
import com.gmail.val59000mc.spigotutils.Parser;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.Map;

public class CastlesListener extends HCListener {

    private Map<HCTeam, CastleBounds> castles;

    public CastlesListener() {
        this.castles = new HashMap<>();
    }

    @EventHandler
    public void afterLoad(HCAfterLoadEvent e) {
        FileConfiguration cfg = getApi().getConfig();
        World world = getApi().getWorldConfig().getWorld();

        HCTeam redTeam = getPmApi().getHCTeam(Constants.RED_TEAM);
        String redMin = cfg.getString("castle-bounds.red.min");
        String redMax = cfg.getString("castle-bounds.red.max");
        CastleBounds redCastle = new CastleBounds(
            redTeam,
            Locations.toBlockLoc(Parser.parseLocation(world, redMin)),
            Locations.toBlockLoc(Parser.parseLocation(world, redMax))
        );

        HCTeam greenTeam = getPmApi().getHCTeam(Constants.GREEN_TEAM);
        String greenMin = cfg.getString("castle-bounds.green.min");
        String greenMax = cfg.getString("castle-bounds.green.max");
        CastleBounds greenCastle = new CastleBounds(
            greenTeam,
            Locations.toBlockLoc(Parser.parseLocation(world, greenMin)),
            Locations.toBlockLoc(Parser.parseLocation(world, greenMax))
        );


        castles.put(redTeam, greenCastle);
        Log.info("Registered forbidden castle for red team" + castles.get(redTeam).toString());

        castles.put(greenTeam, redCastle);
        Log.info("Registered forbidden castle for green team" + castles.get(greenTeam).toString());
    }

    // prevent breaking blocks in enemy castle
    @EventHandler(ignoreCancelled = true)
    public void onBreakCastleBlock(BlockBreakEvent e) {
        if (isDamagingNonOwnedCastle(e.getBlock(), e.getPlayer())) {
            e.setCancelled(true);
        }
    }

    // prevent placing blocks in enemy castle
    @EventHandler(ignoreCancelled = true)
    public void onPlaceCastleBlock(BlockPlaceEvent e) {
        if (isDamagingNonOwnedCastle(e.getBlock(), e.getPlayer())) {
            e.setCancelled(true);
        }
    }

    private boolean isDamagingNonOwnedCastle(Block block, Player player) {
        CastleBounds damagedCastle = getDamagedCastle(block.getLocation());
        if (damagedCastle != null) {
            HCPlayer hcPlayer = getPmApi().getHCPlayer(player);

            if (!hcPlayer.hasTeam()
                || !hcPlayer.is(PlayerState.PLAYING)
                || !hcPlayer.isTeam(damagedCastle.getOwnerTeam())) {
                return true;
            }
        }
        return false;
    }

    // prevent any castle explosion
    @EventHandler(ignoreCancelled = true)
    public void onBlockExplode(BlockExplodeEvent e) {
        for (Block block : e.blockList()) {
            if (getDamagedCastle(block.getLocation()) != null) {
                e.setCancelled(true);
                break;
            }
        }
    }

    // prevent any castle explosion
    @EventHandler(ignoreCancelled = true)
    public void onEntityExplode(EntityExplodeEvent e) {
        for (Block block : e.blockList()) {
            if (getDamagedCastle(block.getLocation()) != null) {
                e.setCancelled(true);
                break;
            }
        }
    }

    private CastleBounds getDamagedCastle(Location location) {
        for (CastleBounds castle : castles.values()) {
            if (castle.getBounds().contains(location)) {
                return castle;
            }
        }
        return null;
    }

    // prevent enter in ennemy castle
    @EventHandler
    public void onEnterCastle(PlayerMoveEvent e) {
        Location from = e.getFrom();
        Location to = e.getTo();

        if (Math.abs(from.getX() - to.getX()) != 0 || Math.abs(from.getZ() - to.getZ()) != 0) {
            HCPlayer hcPlayer = getPmApi().getHCPlayer(e.getPlayer());

            if (hcPlayer.hasTeam() && hcPlayer.is(PlayerState.PLAYING) && castles.get(hcPlayer.getTeam()).getBounds().contains(e.getTo())) {
                e.setTo(e.getFrom());
                Vector pushTowards = from.toVector().subtract(to.toVector());
                e.getPlayer().setVelocity(pushTowards.setY(0.1).normalize().multiply(1));
                getSoundApi().play(hcPlayer, Sound.ENTITY_VILLAGER_NO, 0.5f, 2f);
                getStringsApi().get("lostworld.castle-protection.enter").sendChat(hcPlayer);
            }
        }

    }

    // prevent throwing enderpearl into ennemy castle
    @EventHandler
    public void onThrowEnderpearlInCastle(PlayerTeleportEvent e) {
        Location from = e.getFrom();
        Location to = e.getTo();

        if (to != null && (Math.abs(from.getX() - to.getX()) != 0 || Math.abs(from.getZ() - to.getZ()) != 0)) {
            HCPlayer hcPlayer = getPmApi().getHCPlayer(e.getPlayer());

            if (hcPlayer.hasTeam() && hcPlayer.is(PlayerState.PLAYING) && castles.get(hcPlayer.getTeam()).getBounds().contains(to)) {
                e.setCancelled(true);
                getSoundApi().play(hcPlayer, Sound.ENTITY_VILLAGER_NO, 0.5f, 2f);
                getStringsApi().get("lostworld.castle-protection.enter").sendChat(hcPlayer);
            }
        }

    }

}
