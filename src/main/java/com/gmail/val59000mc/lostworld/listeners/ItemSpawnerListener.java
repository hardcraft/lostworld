package com.gmail.val59000mc.lostworld.listeners;

import com.gmail.val59000mc.hcgameslib.api.HCPlayersManagerAPI;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.spigotutils.Parser;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.BoundingBox;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ItemSpawnerListener extends HCListener {

    @EventHandler
    public void afterLoad(HCAfterLoadEvent e) {
        ConfigurationSection spawners = getApi().getConfig().getConfigurationSection("spawners");
        if (spawners != null) {
            Stream.of(
                parseSpawner(spawners.getConfigurationSection("arrow"), Material.ARROW),
                parseSpawner(spawners.getConfigurationSection("tnt"), Material.TNT),
                parseSpawner(spawners.getConfigurationSection("iron"), Material.IRON_INGOT),
                parseSpawner(spawners.getConfigurationSection("enderpearl"), Material.ENDER_PEARL)
            )
                .filter(Objects::nonNull)
                .forEach(this::scheduleSpawner);
        }
    }

    private SpawnerConfig parseSpawner(ConfigurationSection cfg, Material material) {
        if (cfg == null) {
            return null;
        }
        World world = getApi().getWorldConfig().getWorld();
        List<Location> locations = cfg.getStringList("locations")
            .stream()
            .map(str -> Parser.parseLocation(world, str))
            .collect(Collectors.toList());
        int amount = cfg.getInt("amount", 1);
        int delay = cfg.getInt("delay", 1);
        return new SpawnerConfig(locations, material, amount, delay);
    }

    private void scheduleSpawner(SpawnerConfig spawner) {
        HCPlayersManagerAPI pmApi = getApi().getPlayersManagerAPI();
        Map<Location, BoundingBox> bboxes = spawner.getLocations()
            .stream()
            .collect(Collectors.toMap(l -> l, l -> BoundingBox.of(l, 5, 5, 5)));
        Log.info("Scheduling spawner " + spawner.material.name());
        getApi().buildTask("spawner config " + spawner.material.name(), (task) -> {

            bboxes.forEach((l, bbox) -> {
                l.getWorld().getNearbyEntities(bbox)
                    .stream()
                    .filter(entity -> {
                        HCPlayer hcPlayer = pmApi.getHCPlayer(entity);
                        return hcPlayer != null && hcPlayer.isPlaying();
                    })
                    .findFirst()
                    .ifPresent(entity -> {
                        l.getWorld().dropItem(l, spawner.buildItemStack());
                    });
            });
        })
            .withSecondsInterval(spawner.getDelay())
            .build()
            .start();
    }

    private static class SpawnerConfig {
        private final List<Location> locations;
        private final Material material;
        private final int amount;
        private final int delay;

        private SpawnerConfig(List<Location> locations, Material material, int amount, int delay) {
            this.locations = locations;
            this.material = material;
            this.amount = amount;
            this.delay = delay;
        }

        public List<Location> getLocations() {
            return locations;
        }

        public ItemStack buildItemStack() {
            ItemStack itemStack = new ItemStack(material);
            itemStack.setAmount(amount);
            return itemStack;
        }

        public int getDelay() {
            return delay;
        }
    }
}
