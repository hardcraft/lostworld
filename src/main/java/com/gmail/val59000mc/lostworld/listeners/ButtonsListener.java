package com.gmail.val59000mc.lostworld.listeners;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.lostworld.buttons.ButtonPlateAction;
import com.gmail.val59000mc.lostworld.buttons.JumpPlateAction;
import com.gmail.val59000mc.lostworld.buttons.PotionEffectButtonAction;
import com.gmail.val59000mc.lostworld.buttons.RandomTeleportButtonAction;
import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;
import com.gmail.val59000mc.spigotutils.Locations;
import com.gmail.val59000mc.spigotutils.Parser;

public class ButtonsListener extends HCListener{

	private Map<Location, ButtonPlateAction> buttons;
	private Map<Location, ButtonPlateAction> plates;
	
	public ButtonsListener(){
		this.buttons = new HashMap<>();
		this.plates = new HashMap<>();
	}
	
	@EventHandler
	public void afterLoad(HCAfterLoadEvent e){
		FileConfiguration cfg = getApi().getConfig();
		loadPotionEffectButtonActions(cfg.getConfigurationSection("buttons.jump-boost"));
		loadPotionEffectButtonActions(cfg.getConfigurationSection("buttons.strength"));
		loadPotionEffectButtonActions(cfg.getConfigurationSection("buttons.speed"));
		loadPotionEffectButtonActions(cfg.getConfigurationSection("buttons.absorption"));
		loadRandomTeleportationButtonActions(cfg.getConfigurationSection("buttons.random-teleportation"));
		loadJumpCastlePlatesActions(cfg.getConfigurationSection("plates.jump-castle"));
	}
	
	private void loadJumpCastlePlatesActions(ConfigurationSection cfg) {
		if(cfg == null){
			Log.warn("Couldn't load jump castle platesfrom null configuration section");
			return;
		}
		
		World world = getApi().getWorldConfig().getWorld();
				
		List<String> locStr = cfg.getStringList("locations");
		if(locStr != null){
			for(String str : locStr){
				String[] strLevel = str.split(Pattern.quote(":"));
				Location loc = Locations.toBlockLoc(Parser.parseLocation(world, strLevel[0]));
				int level = Integer.parseInt(strLevel[1]);
				plates.put(loc, new JumpPlateAction(getApi(), loc, level));
			}
		}
	}
	
	
	private void loadRandomTeleportationButtonActions(ConfigurationSection cfg) {
		if(cfg == null){
			Log.warn("Couldn't load random teleportation button from null configuration section");
			return;
		}
		
		World world = getApi().getWorldConfig().getWorld();

		List<Location> destinations = new ArrayList<>();
		List<String> destStr = cfg.getStringList("destinations");
		if(destStr == null || destStr.isEmpty()){
			Log.warn("Couldn't load random teleportation button with empty destinations locations");
			return;
		}
		
		for(String str : destStr){
			destinations.add(Parser.parseLocation(world, str));
		}
		
		List<String> locStr = cfg.getStringList("locations");
		if(locStr != null){
			for(String str : locStr){
				Location loc = Locations.toBlockLoc(Parser.parseLocation(world, str));
				buttons.put(loc, new RandomTeleportButtonAction(getApi(), loc, destinations));
			}
		}
		
	}

	private void loadPotionEffectButtonActions(ConfigurationSection cfg){

		if(cfg == null){
			Log.warn("Couldn't load potion effect button from null configuration section");
			return;
		}
		
		World world = getApi().getWorldConfig().getWorld();
		
		int amplifier = cfg.getInt("amplifier", 0);
		int duration = 20 * cfg.getInt("seconds", 1);
		PotionEffectType type = PotionEffectType.getByName(cfg.getString("type"));

		if(type == null){
			Log.warn("Couldn't load potion effect button "+cfg.getName()+" , missing potion effect type");
			return;
		}
		
		PotionEffect effect = new PotionEffect(type, duration, amplifier);
		List<String> locStr = cfg.getStringList("locations");
		if(locStr != null){
			for(String str : locStr){
				Location loc = Locations.toBlockLoc(Parser.parseLocation(world, str));
				buttons.put(loc, new PotionEffectButtonAction(getApi(), loc, effect));
			}
		}
		
	}
	
	@EventHandler(ignoreCancelled = true)
	public void onButtonClick(PlayerInteractEvent e){
		if(e.getAction() == Action.RIGHT_CLICK_BLOCK && e.getClickedBlock().getType().equals(Material.STONE_BUTTON)){
			Location location = Locations.toBlockLoc(e.getClickedBlock().getLocation());
			ButtonPlateAction buttonAction = buttons.get(location);
			if(buttonAction != null){
				LostWorldPlayer lwPlayer = (LostWorldPlayer) getPmApi().getHCPlayer(e.getPlayer());
				if(lwPlayer != null){
					buttonAction.apply(lwPlayer);
				}
			}
		}
	}
	
	@EventHandler(ignoreCancelled = true)
	public void onPressurePlate(PlayerInteractEvent e){
		if(e.getAction() == Action.PHYSICAL && e.getClickedBlock().getType().equals(Material.STONE_PRESSURE_PLATE)){
			Location location = Locations.toBlockLoc(e.getClickedBlock().getLocation());
			ButtonPlateAction plateAction = plates.get(location);
			if(plateAction != null){
				LostWorldPlayer lwPlayer = (LostWorldPlayer) getPmApi().getHCPlayer(e.getPlayer());
				if(lwPlayer != null){
					plateAction.apply(lwPlayer);
				}
			}
		}
	}
	
	// prevent break button or pressure plates
	@EventHandler(ignoreCancelled = true)
	public void onBreak(BlockBreakEvent e){
		Location location = Locations.toBlockLoc(e.getBlock().getLocation());
		if(isBreakingButtonOrPlate(location)){
			e.setCancelled(true);
		}
	}
	
	// prevent place block or pressure plates
	@EventHandler(ignoreCancelled = true)
	public void onPlace(BlockPlaceEvent e){
		Location location = Locations.toBlockLoc(e.getBlock().getLocation());
		if(isBreakingButtonOrPlate(location)){
			e.setCancelled(true);
		}
	}
	
	// prevent move block with piston
	@EventHandler(ignoreCancelled = true)
	public void onPiston(BlockPistonExtendEvent e){
		for(Block block : e.getBlocks()){
			Location location = Locations.toBlockLoc(block.getRelative(e.getDirection()).getLocation());
			if(isBreakingButtonOrPlate(location)){
				e.setCancelled(true);
				break;
			}
		}
	}
	
	// prevent move block with piston
	@EventHandler(ignoreCancelled = true)
	public void onPiston(BlockPistonRetractEvent e){
		for(Block block : e.getBlocks()){
			Location location = Locations.toBlockLoc(block.getRelative(e.getDirection()).getLocation());
			if(isBreakingButtonOrPlate(location)){
				e.setCancelled(true);
				break;
			}
		}
	}
	
	@EventHandler(ignoreCancelled = true)
	public void onExplode(BlockExplodeEvent e){
		for(Block block : e.blockList()){
			Location location = Locations.toBlockLoc(block.getLocation());
			if(isBreakingButtonOrPlate(location)){
				e.setCancelled(true);
				break;
			}
		}
	}
	
	@EventHandler(ignoreCancelled = true)
	public void onExplode(EntityExplodeEvent e){
		for(Block block : e.blockList()){
			Location location = Locations.toBlockLoc(block.getLocation());
			if(isBreakingButtonOrPlate(location)){
				e.setCancelled(true);
				break;
			}
		}
	}
	
	private boolean isBreakingButtonOrPlate(Location location){
		return checkLocation(location)
			|| checkLocation(location.clone().add(1, 0, 0))
			|| checkLocation(location.clone().add(-1, 0, 0))
			|| checkLocation(location.clone().add(0, 1, 0))
			|| checkLocation(location.clone().add(0, -1, 0))
			|| checkLocation(location.clone().add(0, 0, 1))
			|| checkLocation(location.clone().add(0, 0, -1));
	}
	
	private boolean checkLocation(Location location){
		return buttons.containsKey(location) || plates.containsKey(location);
	}
}
