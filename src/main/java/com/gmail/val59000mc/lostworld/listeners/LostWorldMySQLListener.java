package com.gmail.val59000mc.lostworld.listeners;

import com.gmail.val59000mc.hcgameslib.listeners.HCListener;

public class LostWorldMySQLListener extends HCListener{

	// Queries
	private String createLostWorldPlayerSQL;
	private String createLostWorldPlayerStatsSQL;
	private String insertLostWorldPlayerSQL;
	private String insertLostWorldPlayerStatsSQL;
	private String updateLostWorldPlayerGlobalStatsSQL;
	private String updateLostWorldPlayerStatsSQL;
	
//	/**
//	 * Load sql queries from resources files
//	 */
//	public void readQueries() {
//		HCMySQLAPI sql = getMySQLAPI();
//		if(sql.isEnabled()){
//			createLostWorldPlayerSQL = sql.readQueryFromResource(getApi().getPlugin(),"resources/sql/create_lostworld_player.sql");
//			createLostWorldPlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(),"resources/sql/create_lostworld_player_stats.sql");
//			insertLostWorldPlayerSQL = sql.readQueryFromResource(getApi().getPlugin(),"resources/sql/insert_lostworld_player.sql");
//			insertLostWorldPlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(),"resources/sql/insert_lostworld_player_stats.sql");
//			updateLostWorldPlayerGlobalStatsSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/update_lostworld_player_global_stats.sql");
//			updateLostWorldPlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/update_lostworld_player_stats.sql");
//		}
//	}
//	
//	/**
//	 * Insert game if not exists
//	 * @param e
//	 */
//	@EventHandler
//	public void onGameFinishedLoading(HCAfterLoadEvent e){
//		HCMySQLAPI sql = getMySQLAPI();
//		
//		readQueries();
//		
//		if(sql.isEnabled()){
//			
//			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
//				@Override
//				public void run() {
//					
//					try{
//						
//						sql.execute(sql.prepareStatement(createLostWorldPlayerSQL));
//						
//						sql.execute(sql.prepareStatement(createLostWorldPlayerStatsSQL));
//						
//						
//					}catch(SQLException e){
//						Logger.severe("Couldnt create tables for LostWorld stats or perks");
//						e.printStackTrace();
//					}
//					
//				}
//			});
//		}
//	}
//	
//	/**
//	 * Add new player if not exists when joining
//	 * Update name (because it may change)
//	 * Update current played game 
//	 * @param e
//	 */
//	@EventHandler(priority=EventPriority.HIGHEST)
//	public void onPlayerJoin(HCPlayerDBInsertedEvent e){
//		HCMySQLAPI sql = getMySQLAPI();
//		
//		if(sql.isEnabled()){
//			
//			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
//				@Override
//				public void run() {
//
//					try{ 
//						String id = String.valueOf(e.getHcPlayer().getId());
//						
//						// Insert lostWorld player if not exists
//						sql.execute(sql.prepareStatement(insertLostWorldPlayerSQL, id));
//						
//						// Insert lostWorld player stats if not exists
//						sql.execute(sql.prepareStatement(insertLostWorldPlayerStatsSQL, id, sql.getMonth(), sql.getYear()));
//						
//					} catch (SQLException ex) {
//						Log.severe("Couldn't find perks for player "+e.getHcPlayer().getName());
//						ex.printStackTrace();
//					}
//				}
//			});
//		}
//	}
//	
//	
//	/**
//	 * Save all players global data when game ends
//	 * @param e
//	 */
//	@EventHandler
//	public void onGameEnds(HCPlayerDBPersistedSessionEvent e){
//		HCMySQLAPI sql = getMySQLAPI();
//		
//		if(sql.isEnabled()){
//			
//			LostWorldPlayer lostWorldPlayer = (LostWorldPlayer) e.getHcPlayer();
//			
//			// Launch one async task to save all data
//			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
//				@Override
//				public void run() {
//
//					try{
//						// Update lostWorld player global stats
//						sql.execute(sql.prepareStatement(updateLostWorldPlayerGlobalStatsSQL,
//								String.valueOf(lostWorldPlayer.getZombiesKilled()),
//								String.valueOf(lostWorldPlayer.getTrophiesCollected()),
//								String.valueOf(lostWorldPlayer.getObsidiansBroken()),
//								String.valueOf(lostWorldPlayer.getBobCollected()),
//								String.valueOf(lostWorldPlayer.getFirstToReachCastle()),
//								String.valueOf(lostWorldPlayer.getTimePlayed()),
//								String.valueOf(lostWorldPlayer.getKills()),
//								String.valueOf(lostWorldPlayer.getDeaths()),
//								String.valueOf(lostWorldPlayer.getMoney()),
//								String.valueOf(lostWorldPlayer.getWins()),
//								String.valueOf(lostWorldPlayer.getId())
//						));
//						
//	
//						// Update lostWorld player stats
//						sql.execute(sql.prepareStatement(updateLostWorldPlayerStatsSQL,
//								String.valueOf(lostWorldPlayer.getZombiesKilled()),
//								String.valueOf(lostWorldPlayer.getTrophiesCollected()),
//								String.valueOf(lostWorldPlayer.getObsidiansBroken()),
//								String.valueOf(lostWorldPlayer.getBobCollected()),
//								String.valueOf(lostWorldPlayer.getFirstToReachCastle()),
//								String.valueOf(lostWorldPlayer.getTimePlayed()),
//								String.valueOf(lostWorldPlayer.getKills()),
//								String.valueOf(lostWorldPlayer.getDeaths()),
//								String.valueOf(lostWorldPlayer.getMoney()),
//								String.valueOf(lostWorldPlayer.getWins()),
//								String.valueOf(lostWorldPlayer.getId()),
//								sql.getMonth(),
//								sql.getYear()
//						));
//					
//					}catch(SQLException e){
//						Logger.severe("Couldnt update LostWorld player stats for player="+lostWorldPlayer.getName()+" uuid="+lostWorldPlayer.getUuid());
//						e.printStackTrace();
//					}
//					
//				}
//			});
//		}
//
//	}
}
