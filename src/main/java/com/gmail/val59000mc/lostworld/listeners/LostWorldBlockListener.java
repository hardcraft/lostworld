package com.gmail.val59000mc.lostworld.listeners;

import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockGrowEvent;

public class LostWorldBlockListener extends HCListener {

    // prevent vine growth
    @EventHandler
    public void onVineGrow(BlockGrowEvent e) {
        e.setCancelled(true);
    }

    // prevent loot obsidian block
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBreakObsidian(BlockBreakEvent e) {
        if (e.getBlock().getType().equals(Material.OBSIDIAN)) {
            e.getBlock().setType(Material.AIR);
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBreakSpawner(BlockBreakEvent e) {
        if (e.getBlock().getType().equals(Material.SPAWNER)) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onExploseSpawner(BlockExplodeEvent e) {
        if (e.getBlock().getType().equals(Material.SPAWNER)) {
            e.setCancelled(true);
        }
    }

}
