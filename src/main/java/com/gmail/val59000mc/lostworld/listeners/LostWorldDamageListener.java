package com.gmail.val59000mc.lostworld.listeners;

import com.gmail.val59000mc.hcgameslib.events.HCPlayerDamagedByHCPlayerEvent;
import com.gmail.val59000mc.hcgameslib.game.GameState;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import org.bukkit.entity.EnderPearl;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

public class LostWorldDamageListener extends HCListener {

    @EventHandler(priority = EventPriority.HIGH)
    public void onEnderpearlDamage(HCPlayerDamagedByHCPlayerEvent e) {
        if (getApi().is(GameState.PLAYING)) {
            if (e.getWrapped().getDamager() instanceof EnderPearl) {
                e.getWrapped().setCancelled(false);
            }
        }
    }

}
