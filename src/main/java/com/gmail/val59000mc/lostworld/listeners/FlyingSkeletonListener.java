package com.gmail.val59000mc.lostworld.listeners;

import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.events.HCAfterPlayEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.lostworld.common.Constants;
import com.gmail.val59000mc.spigotutils.Parser;
import io.lumine.xikage.mythicmobs.MythicMobs;
import io.lumine.xikage.mythicmobs.api.bukkit.BukkitAPIHelper;
import io.lumine.xikage.mythicmobs.api.exceptions.InvalidMobTypeException;
import io.lumine.xikage.mythicmobs.mobs.MythicMob;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.event.EventHandler;

import java.util.ArrayList;
import java.util.List;

public class FlyingSkeletonListener extends HCListener {

    private List<Location> flyingSkeletonLocations;

    public FlyingSkeletonListener() {
        this.flyingSkeletonLocations = new ArrayList<Location>();
    }

    @EventHandler
    public void afterLoad(HCAfterLoadEvent e) {
        List<String> locStrList = getApi().getConfig().getStringList("flying-skeletons");
        if (locStrList != null) {
            World world = getApi().getWorldConfig().getWorld();
            for (String locStr : locStrList) {
                flyingSkeletonLocations.add(Parser.parseLocation(world, locStr));
            }
        }
    }

    @EventHandler
    public void afterPlay(HCAfterPlayEvent e) {
        BukkitAPIHelper mobApi = MythicMobs.inst().getAPIHelper();
        for (Location location : flyingSkeletonLocations) {
            try {
                MythicMob flyingSkeleton = mobApi.getMythicMob(Constants.FLYING_SKELETON_INTERNAL_NAME);
                mobApi.spawnMythicMob(flyingSkeleton, location, 1);
            } catch (InvalidMobTypeException ex) {
                throw new IllegalStateException("Could not spawn Flying Skeleton mythic mob", ex);
            }
        }
    }
}
