package com.gmail.val59000mc.lostworld.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

import com.gmail.val59000mc.hcgameslib.events.HCPlayerDamagedByHCPlayerEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerDamagedByPotionEvent;
import com.gmail.val59000mc.hcgameslib.game.GameState;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.lostworld.events.ObjectiveCompletedEvent;
import com.gmail.val59000mc.lostworld.objectives.ObjectiveType;

/**
 * No PVP when walking toward castle before objective is completed
 * @author Valentin
 *
 */
public class NoPvpBeforeCastleListener extends HCListener{

	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPvp(HCPlayerDamagedByHCPlayerEvent e){
		if(getApi().is(GameState.PLAYING)){
			e.getWrapped().setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPotionPvp(HCPlayerDamagedByPotionEvent e){
		if(getApi().is(GameState.PLAYING)){
			e.getWrapped().setCancelled(true);
		}
	}
	
	// enable PVP after castle objective is completed
	@EventHandler
	public void onCastleObjetiveCompletion(ObjectiveCompletedEvent e){
		if(e.getObjective().getType().equals(ObjectiveType.CASTLE)){
			getApi().unregisterListener(this);
		}
	}
	
}
