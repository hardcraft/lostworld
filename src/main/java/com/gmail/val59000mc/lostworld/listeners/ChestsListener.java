package com.gmail.val59000mc.lostworld.listeners;

import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.lostworld.callbacks.LostWorldCallbacks;
import com.gmail.val59000mc.lostworld.events.FillChestEvent;
import com.gmail.val59000mc.lostworld.items.ChestsManager;
import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;
import com.gmail.val59000mc.spigotutils.Locations;
import org.bukkit.*;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class ChestsListener extends HCListener {

    private ChestsManager chestsManager;

    @EventHandler(priority = EventPriority.NORMAL)
    public void afterLoad(HCAfterLoadEvent e) {
        this.chestsManager = new ChestsManager(getApi());
        ((LostWorldCallbacks) getCallbacksApi()).setChestsManager(chestsManager);
    }

    @EventHandler
    public void onPlaceChest(FillChestEvent e) {


        Chest chest = chestsManager.placeNextRandomChestInWorld();
        if (chest == null) {
            return;
        }

        Location location = Locations.toBlockLoc(chest.getLocation());

        getStringsApi().get("lostworld.items.random-filled-chest.placed")
            .replace("%x%", String.valueOf(location.getBlockX()))
            .replace("%y%", String.valueOf(location.getBlockY()))
            .replace("%z%", String.valueOf(location.getBlockZ()))
            .sendChatP();

        getStringsApi().get("lostworld.items.random-filled-chest.placed-title")
            .sendTitle(20, 50, 20);

        getApi().buildTask("supply alert sound", task -> getSoundApi().play(Sound.BLOCK_NOTE_BLOCK_PLING, 1, 2))
            .withInterval(5)
            .withIterations(7)
            .build()
            .start();

        getApi().buildTask("display line of fire for filled chest", new HCTask() {

            private final List<Location> particlesLocations;
            private final Vector particleDirection;

            {

                // initialize particles locations
                Location chestCenter = location.clone().add(0.5, 0.5, 0.5);
                particlesLocations = new ArrayList<>();
                for (double i = 0; i < 300.d; i += 1d) {
                    particlesLocations.add(chestCenter.clone().add(0d, i, 0d));
                }

                // initialize particle direction
                particleDirection = new Vector(0, 0, 0);
            }

            @Override
            public void run() {
                if (!location.getBlock().getType().equals(Material.CHEST)) {
                    getSoundApi().play(Sound.BLOCK_CHEST_OPEN, 2, 1);
                    getStringsApi().get("lostworld.items.random-filled-chest.destroyed")
                        .replace("%x%", String.valueOf(location.getBlockX()))
                        .replace("%y%", String.valueOf(location.getBlockY()))
                        .replace("%z%", String.valueOf(location.getBlockZ()))
                        .sendChatP();
                    scheduler.stop();
                }

                for (Location particleLocation : particlesLocations) {
                    World world = particleLocation.getWorld();
                    world.spawnParticle(Particle.FLAME, particleLocation, 1, particleDirection.getX(), particleDirection.getY(), particleDirection.getZ(), 0.1, null, true);
                }
            }
        })
            .addListener(new HCTaskListener() {

                @EventHandler(ignoreCancelled = true)
                public void onOpenChest(PlayerInteractEvent e) {
                    if (e.getAction() == Action.RIGHT_CLICK_BLOCK && e.getClickedBlock().getType().equals(Material.CHEST)) {
                        Location chestLocation = Locations.toBlockLoc(e.getClickedBlock().getLocation());
                        if (isCorrectChest(chestLocation)) {
                            if (checkAndBroadcastPlayingPlayer(e.getPlayer())) {
                                e.getClickedBlock().setType(Material.AIR);
                                getScheduler().stop();
                            } else {
                                e.setCancelled(true);
                            }
                        }
                    }
                }

                @EventHandler(ignoreCancelled = true)
                public void onBreakChest(BlockBreakEvent e) {
                    if (e.getBlock().getType().equals(Material.CHEST)) {
                        Location chestLocation = Locations.toBlockLoc(e.getBlock().getLocation());
                        if (isCorrectChest(chestLocation)) {
                            if (checkAndBroadcastPlayingPlayer(e.getPlayer())) {
                                e.getBlock().setType(Material.AIR);
                                getScheduler().stop();
                            } else {
                                e.setCancelled(true);
                            }
                        }
                    }
                }

                private boolean isCorrectChest(Location chestLocation) {
                    return location.equals(chestLocation);
                }

                private boolean checkAndBroadcastPlayingPlayer(Player player) {
                    LostWorldPlayer lwPlayer = (LostWorldPlayer) getPmApi().getHCPlayer(player);
                    if (lwPlayer != null && lwPlayer.isPlaying()) {
                        getSoundApi().play(Sound.BLOCK_CHEST_OPEN, 2, 1);
                        getStringsApi().get("lostworld.items.random-filled-chest.opened-by-player")
                            .replace("%player%", lwPlayer.getColoredName())
                            .replace("%x%", String.valueOf(location.getBlockX()))
                            .replace("%y%", String.valueOf(location.getBlockY()))
                            .replace("%z%", String.valueOf(location.getBlockZ()))
                            .sendChatP();
                        return true;
                    }
                    return false;
                }

            })
            .withInterval(20)
            .withIterations(-1)
            .build()
            .start();
    }

}
