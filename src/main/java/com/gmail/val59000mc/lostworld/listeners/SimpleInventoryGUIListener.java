package com.gmail.val59000mc.lostworld.listeners;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.event.EventHandler;

import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.lostworld.collects.CollectBobHeadAction;
import com.gmail.val59000mc.lostworld.collects.CollectTrophyAction;
import com.gmail.val59000mc.lostworld.common.Constants;
import com.gmail.val59000mc.lostworld.players.LostWorldTeam;
import com.gmail.val59000mc.simpleinventorygui.events.SIGRegisterTriggerEvent;
import com.gmail.val59000mc.simpleinventorygui.triggers.RightClickEntityUUIDTrigger;
import com.gmail.val59000mc.simpleinventorygui.triggers.TriggerManager;
import com.google.common.collect.Lists;

public class SimpleInventoryGUIListener extends HCListener{

		
	@EventHandler
	public void onSIGLoad(SIGRegisterTriggerEvent e){
		TriggerManager tm = e.getTm();
	
		LostWorldTeam redTeam = (LostWorldTeam) getPmApi().getHCTeam(Constants.RED_TEAM);
		
		UUID redTrophy = UUID.fromString(getApi().getConfig().getString("objectives.TROPHIES.armor-stands.red"));
		tm.addTrigger(
			new RightClickEntityUUIDTrigger(
				new ArrayList<>(), 
				0, 
				null, 
				Lists.newArrayList(new CollectTrophyAction(getApi(), redTeam)), 
				redTrophy
			)
		);
		
		UUID redBob = UUID.fromString(getApi().getConfig().getString("objectives.BOB.armor-stands.red"));
		tm.addTrigger(
			new RightClickEntityUUIDTrigger(
				new ArrayList<>(), 
				0, 
				null, 
				Lists.newArrayList(new CollectBobHeadAction(getApi(), redTeam)), 
				redBob
			)
		);
		

		LostWorldTeam greenTeam = (LostWorldTeam) getPmApi().getHCTeam(Constants.GREEN_TEAM);
		
		UUID greenTrophy = UUID.fromString(getApi().getConfig().getString("objectives.TROPHIES.armor-stands.green"));
		tm.addTrigger(
			new RightClickEntityUUIDTrigger(
				new ArrayList<>(), 
				0, 
				null, 
				Lists.newArrayList(new CollectTrophyAction(getApi(), greenTeam)), 
				greenTrophy
			)
		);
		
		UUID greenBob = UUID.fromString(getApi().getConfig().getString("objectives.BOB.armor-stands.green"));
		tm.addTrigger(
			new RightClickEntityUUIDTrigger(
				new ArrayList<>(), 
				0, 
				null, 
				Lists.newArrayList(new CollectBobHeadAction(getApi(), greenTeam)), 
				greenBob
			)
		);
	}
	
	
}
