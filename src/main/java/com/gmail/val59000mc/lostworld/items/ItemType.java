package com.gmail.val59000mc.lostworld.items;

import org.bukkit.Material;

public enum ItemType {
    HELMETS(Material.IRON_HELMET, Material.DIAMOND_HELMET),
    CHESTPLATES(Material.IRON_CHESTPLATE, Material.DIAMOND_CHESTPLATE),
    LEGGINGS(Material.IRON_LEGGINGS, Material.DIAMOND_LEGGINGS),
    BOOTS(Material.IRON_BOOTS, Material.DIAMOND_BOOTS),
    SWORDS(Material.IRON_SWORD, Material.DIAMOND_SWORD),
    BOWS(Material.BOW),
    TOOLS(Material.DIAMOND_AXE, Material.DIAMOND_PICKAXE),
    POTIONS(Material.POTION, Material.SPLASH_POTION, Material.LINGERING_POTION),
    FOOD(Material.GOLDEN_APPLE),
    OTHER(Material.ENDER_PEARL, Material.COBWEB, Material.TNT, Material.DIAMOND, Material.IRON_INGOT, Material.DIAMOND_BLOCK, Material.IRON_BLOCK, Material.LADDER, Material.VINE);

    private Material[] materials;

    private ItemType(Material... materials) {
        this.materials = materials;
    }

    public Material[] getMaterials() {
        return materials;
    }
}
