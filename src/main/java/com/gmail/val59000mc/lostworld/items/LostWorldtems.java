package com.gmail.val59000mc.lostworld.items;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.api.HCStringsAPI;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.lostworld.players.LostWorldTeam;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.ItemCompareOption;
import com.gmail.val59000mc.spigotutils.items.ItemBuilder;
import com.gmail.val59000mc.spigotutils.items.LeatherArmorMetaBuilder;
import com.google.common.collect.Lists;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

public class LostWorldtems {

    private static HCGameAPI api;
    private static HCStringsAPI s;

    public static void setup(HCGameAPI gameApi) {
        api = gameApi;
        s = gameApi.getStringsAPI();
    }

    public static Color getBukkitColor(LostWorldTeam team) {
        switch (team.getColor()) {
            case RED:
                return Color.fromRGB(255, 0, 0);
            default:
            case GREEN:
                return Color.fromRGB(0, 255, 0);
        }
    }

    public static List<ItemStack> getArmor(HCPlayer hcPlayer) {
        Color color = getBukkitColor((LostWorldTeam) hcPlayer.getTeam());
        return getArmor(color);
    }

    public static List<ItemStack> getArmor(Color color) {

        return Lists.newArrayList(
            new ItemBuilder(Material.LEATHER_HELMET)
                .buildMeta(LeatherArmorMetaBuilder.class)
                .withColor(color)
                .item()
                .build(),
            new ItemBuilder(Material.LEATHER_CHESTPLATE)
                .buildMeta(LeatherArmorMetaBuilder.class)
                .withColor(color)
                .item()
                .build(),
            new ItemBuilder(Material.LEATHER_LEGGINGS)
                .buildMeta(LeatherArmorMetaBuilder.class)
                .withColor(color)
                .item()
                .build(),
            new ItemBuilder(Material.LEATHER_BOOTS)
                .buildMeta(LeatherArmorMetaBuilder.class)
                .withColor(color)
                .withEnchant(Enchantment.PROTECTION_FALL, 4, true)
                .item()
                .build()
        );
    }

    public static List<ItemStack> getInitialItems() {

        return Lists.newArrayList(
            new ItemBuilder(Material.FISHING_ROD)
                .buildMeta()
                .withEnchant(Enchantment.LURE, 3, true)
                .item()
                .build(),
            new ItemStack(Material.GOLDEN_CARROT, 64),
            new ItemStack(Material.RED_TULIP, 1, (short) 1),
            new ItemStack(Material.COBWEB, 8),
            new ItemStack(Material.SUNFLOWER, 1, (short) 175),
            new ItemStack(Material.IRON_AXE, 1),
            new ItemStack(Material.IRON_PICKAXE, 1)
        );
    }


    public static List<ItemStack> getItems() {

        return Lists.newArrayList(
            new ItemStack(Material.STONE_SWORD),
            new ItemStack(Material.BOW),
            new ItemStack(Material.ENDER_PEARL, 3),
            new ItemBuilder(Material.IRON_PICKAXE)
                .buildMeta()
                .withEnchant(Enchantment.DIG_SPEED, 1, true)
                .withEnchant(Enchantment.DURABILITY, 1, true)
                .item()
                .build(),
            new ItemBuilder(Material.IRON_AXE)
                .buildMeta()
                .withEnchant(Enchantment.DIG_SPEED, 1, true)
                .withEnchant(Enchantment.DURABILITY, 1, true)
                .item()
                .build(),
            new ItemStack(Material.GOLDEN_CARROT, 64),
            new ItemStack(Material.ARROW, 16),
            new ItemStack(Material.OAK_WOOD, 64),
            new ItemStack(Material.REDSTONE_BLOCK, 5)
        );
    }

    public static void filterDrops(List<ItemStack> drops) {
        Iterator<ItemStack> it = drops.iterator();
        while (it.hasNext()) {
            ItemStack item = it.next();
            boolean shouldRemove = false;

            int enchSize = item.getEnchantments().size();

            switch (item.getType()) {
                case BOW:
                case STONE_SWORD:
                case LEATHER_HELMET:
                case LEATHER_CHESTPLATE:
                case LEATHER_LEGGINGS:
                    if (enchSize == 0)
                        shouldRemove = true;
                    break;
                case LEATHER_BOOTS:
                    if (enchSize == 1 && item.getEnchantmentLevel(Enchantment.PROTECTION_FALL) == 4)
                        shouldRemove = true;
                    break;
                case FISHING_ROD:
                    if (enchSize == 1 && item.getEnchantmentLevel(Enchantment.LURE) == 3)
                        shouldRemove = true;
                    break;
                case REDSTONE_BLOCK:
                case COOKED_CHICKEN:
                case GOLDEN_CARROT:
                case RED_TULIP:
                case DANDELION:
                case SUNFLOWER:
                case ENDER_PEARL:
                    shouldRemove = true;
                    break;
                case IRON_AXE:
                case IRON_PICKAXE:
                    if (enchSize == 0 ||
                        (enchSize == 2 && item.getEnchantmentLevel(Enchantment.DIG_SPEED) == 1 && item.getEnchantmentLevel(Enchantment.DURABILITY) == 1))
                        shouldRemove = true;
                    break;
                case OAK_WOOD:
                case ARROW:
                    if (item.getAmount() == 64)
                        shouldRemove = true;
                    break;
                default:
                    // don't mark for removal
                    break;
            }

            if (shouldRemove)
                it.remove();
        }

    }

    public static boolean hasBobHead(Player player) {
        return Inventories.containsAtLeast(
            player.getInventory(),
            getBobHead(),
            1,
            new ItemCompareOption.Builder()
                .withAmount(true)
                .withDisplayName(true)
                .withLore(true)
                .withEnchantments(true)
                .withMaterial(true)
                .build()
        );
    }


    public static void removeBobHead(Player player) {
        Inventories.remove(
            player.getInventory(),
            getBobHead(),
            new ItemCompareOption.Builder()
                .withAmount(false)
                .withDisplayName(true)
                .withLore(true)
                .withMaterial(true)
                .withDamageValue(true)
                .build()
        );
    }

    public static boolean isBobHead(Item item) {
        if (item != null && item.getType().equals(EntityType.DROPPED_ITEM)) {
            return isBobHead(item.getItemStack());
        }
        return false;
    }

    public static boolean isBobHead(ItemStack item) {
        if (item != null
            && item.getType().equals(Material.SPONGE)
            && item.hasItemMeta()
            && item.getItemMeta().getLore() != null) {
            List<String> lore = Lists.newArrayList(api.getStringsAPI().get("lostworld.items.bob-lore").toString().split(Pattern.quote("|")));
            return item.getItemMeta().getLore().containsAll(lore);
        }
        return false;
    }

    public static ItemStack getBobHead() {
        return new ItemBuilder(Material.SPONGE)
            .buildMeta()
            .withEnchant(Enchantment.ARROW_INFINITE, 1, true)
            .withItemFlags(ItemFlag.HIDE_ENCHANTS)
            .withDisplayName(s.get("lostworld.items.bob-name").toString())
            .withLore(api.getStringsAPI().get("lostworld.items.bob-lore").toString().split(Pattern.quote("|")))
            .item()
            .build();
    }

    public static boolean hasTrophyItem(Player player) {
        return Inventories.containsAtLeast(
            player.getInventory(),
            getTrophyItem(1),
            1,
            new ItemCompareOption.Builder()
                .withAmount(false)
                .withDisplayName(true)
                .withLore(true)
                .withMaterial(true)
                .withDamageValue(true)
                .build()
        );
    }

    public static int countTrophyItems(Player player) {
        int count = Inventories.countOccurrences(
            player.getInventory().getContents(),
            getTrophyItem(1),
            new ItemCompareOption.Builder()
                .withAmount(false)
                .withDisplayName(true)
                .withLore(true)
                .withMaterial(true)
                .withDamageValue(true)
                .build()
        );
        count += Inventories.countOccurrences(
            player.getInventory().getArmorContents(),
            getTrophyItem(1),
            new ItemCompareOption.Builder()
                .withAmount(false)
                .withDisplayName(true)
                .withLore(true)
                .withMaterial(true)
                .withDamageValue(true)
                .build()
        );
        return count;
    }

    public static void removeTrophyItems(Player player, int amount) {
        Inventories.remove(
            player.getInventory(),
            getTrophyItem(amount),
            new ItemCompareOption.Builder()
                .withAmount(false)
                .withDisplayName(true)
                .withLore(true)
                .withMaterial(true)
                .withDamageValue(true)
                .build()
        );
    }

    public static boolean isTrophyItem(ItemStack item) {
        if (item != null
            && item.getType().equals(Material.PLAYER_HEAD)
            && item.getDurability() == (short) 3
            && item.hasItemMeta()
            && item.getItemMeta().getLore() != null) {
            List<String> lore = Lists.newArrayList(api.getStringsAPI().get("lostworld.items.trophy-lore").toString().split(Pattern.quote("|")));
            return item.getItemMeta().getLore().containsAll(lore);
        }
        return false;
    }


    public static ItemStack getTrophyItem(int amount) {
        return new ItemBuilder(Material.PLAYER_HEAD)
            .withAmount(amount)
            .withDurability(3)
            .buildMeta()
            .withDisplayName(s.get("lostworld.items.trophy-name").toString())
            .withLore(api.getStringsAPI().get("lostworld.items.trophy-lore").toString().split(Pattern.quote("|")))
            .item()
            .build();
    }
}
