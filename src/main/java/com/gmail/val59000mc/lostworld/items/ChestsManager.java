package com.gmail.val59000mc.lostworld.items;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.api.HCItemsAPI;
import com.gmail.val59000mc.hcgameslib.api.impl.items.ItemStore;
import com.gmail.val59000mc.hcgameslib.api.impl.items.ItemStoreBuilder;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.lostworld.common.Constants;
import com.gmail.val59000mc.spigotutils.Parser;
import com.gmail.val59000mc.spigotutils.Randoms;
import com.gmail.val59000mc.spigotutils.items.PotionTypes;
import com.google.common.collect.Lists;
import com.sucy.enchant.EnchantmentAPI;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class ChestsManager {

    private HCGameAPI api;

    private List<Location> locations;
    private List<Location> remainingLocations;
    private int minItems;
    private int maxItems;

    public ChestsManager(HCGameAPI api) {
        this.api = api;
        this.locations = new ArrayList<>();
        this.remainingLocations = new ArrayList<>();
        load();
    }

    public Chest placeChestInWorld(Location location, List<ItemStack> items) {
        Block block = location.getBlock();
        if (block.getState() instanceof Chest) {
            ((Chest) block.getState()).getInventory().clear();
        }
        block.setType(Material.CHEST);
        Chest chest = (Chest) block.getState();
        Inventory inv = chest.getInventory();
        api.getItemsAPI().randomFillInventory(inv, items);
        return chest;
    }

    public Chest placeNextRandomChestInWorld() {
        if (remainingLocations.isEmpty()) {
            remainingLocations = new ArrayList<>(locations);

            if (remainingLocations.isEmpty()) {
                Log.warn("no more locations available to spawn a chest !. Aborting");
                return null;
            }
        }
        Location nextLocation = remainingLocations.remove(Randoms.randomInteger(0, remainingLocations.size() - 1));
        List<ItemStack> items = getRandomItems(Randoms.randomInteger(minItems, maxItems));

        return placeChestInWorld(nextLocation, items);
    }

    public List<ItemStack> getRandomItems(int amount) {
        return api.getItemsAPI().getItemStore(Constants.ALL_ITEM_STORE).getRandomItems(amount);
    }

    public ItemStack getRandomItem() {
        return api.getItemsAPI().getItemStore(Constants.ALL_ITEM_STORE).getRandomItem();
    }

    public ItemStack getRandomItem(ItemType type) {
        return api.getItemsAPI().getItemStore(Constants.ALL_ITEM_STORE).getRandomItem(type.getMaterials());
    }

    public List<ItemStack> getRandomItem(ItemType type, int amount) {
        return api.getItemsAPI().getItemStore(Constants.ALL_ITEM_STORE).getRandomItems(amount, type.getMaterials());
    }

    private void load() {

        Log.info("Loading items");
        Long start = System.currentTimeMillis();


        HCItemsAPI items = api.getItemsAPI();

        ItemStore store;

        ItemStoreBuilder helmets = items.buildItemStore(ItemType.HELMETS.getMaterials())
            .withAmount(1)
            .withDurabilityPercentages(10, 20, 30, 40, 50, 60, 70, 80, 90, 100)
            .withEnchantmentNumbers(0, 1, 2, 3)
            .withEnchantments(Enchantment.PROTECTION_ENVIRONMENTAL, 1, 2, 3, 4)
            .withEnchantments(Enchantment.PROTECTION_FIRE, 1, 2, 3, 4)
            .withEnchantments(Enchantment.PROTECTION_EXPLOSIONS, 1, 2, 3, 4)
            .withEnchantments(Enchantment.PROTECTION_PROJECTILE, 1, 2, 3, 4)
            .withEnchantments(Enchantment.OXYGEN, 1, 2, 3)
            .withEnchantments(Enchantment.WATER_WORKER, 1, 2, 3)
            .withEnchantments(Enchantment.DURABILITY, 1, 2, 3)
            .withModifier((isb, item) -> {
                addCustomEnchant(item, Lists.newArrayList("brilliance"));
            });
        store = helmets.build(50);


        ItemStoreBuilder chestplates = items.buildItemStore(ItemType.CHESTPLATES.getMaterials())
            .withAmount(1)
            .withDurabilityPercentages(10, 20, 30, 40, 50, 60, 70, 80, 90, 100)
            .withEnchantmentNumbers(0, 1, 2)
            .withEnchantments(Enchantment.PROTECTION_ENVIRONMENTAL, 1, 2, 3, 4)
            .withEnchantments(Enchantment.PROTECTION_FIRE, 1, 2, 3, 4)
            .withEnchantments(Enchantment.PROTECTION_EXPLOSIONS, 1, 2, 3, 4)
            .withEnchantments(Enchantment.PROTECTION_PROJECTILE, 1, 2, 3, 4)
            .withEnchantments(Enchantment.DURABILITY, 1, 2, 3)
            .withModifier((isb, item) -> {
                addCustomEnchant(item, Lists.newArrayList("cursed", "demoralizing", "life", "toxic"));
            });
        chestplates.build(store, 50);


        ItemStoreBuilder leggings = items.buildItemStore(ItemType.LEGGINGS.getMaterials())
            .withAmount(1)
            .withDurabilityPercentages(10, 20, 30, 40, 50, 60, 70, 80, 90, 100)
            .withEnchantmentNumbers(0, 1, 2)
            .withEnchantments(Enchantment.PROTECTION_ENVIRONMENTAL, 1, 2, 3, 4)
            .withEnchantments(Enchantment.PROTECTION_FIRE, 1, 2, 3, 4)
            .withEnchantments(Enchantment.PROTECTION_EXPLOSIONS, 1, 2, 3, 4)
            .withEnchantments(Enchantment.PROTECTION_PROJECTILE, 1, 2, 3, 4)
            .withEnchantments(Enchantment.DURABILITY, 1, 2, 3)
            .withModifier((isb, item) -> {
                addCustomEnchant(item, Lists.newArrayList("adrenaline", "frost"));
            });
        leggings.build(store, 50);


        ItemStoreBuilder boots = items.buildItemStore(ItemType.BOOTS.getMaterials())
            .withAmount(1)
            .withDurabilityPercentages(10, 20, 30, 40, 50, 60, 70, 80, 90, 100)
            .withEnchantmentNumbers(0, 1, 2, 3)
            .withEnchantments(Enchantment.PROTECTION_ENVIRONMENTAL, 1, 2, 3, 4)
            .withEnchantments(Enchantment.PROTECTION_FIRE, 1, 2, 3, 4)
            .withEnchantments(Enchantment.PROTECTION_EXPLOSIONS, 1, 2, 3, 4)
            .withEnchantments(Enchantment.PROTECTION_PROJECTILE, 1, 2, 3, 4)
            .withEnchantments(Enchantment.PROTECTION_FALL, 1, 2, 3, 4)
            .withEnchantments(Enchantment.DEPTH_STRIDER, 1, 2, 3)
            .withEnchantments(Enchantment.DURABILITY, 1, 2, 3)
            .withModifier((isb, item) -> {
                addCustomEnchant(item, Lists.newArrayList("jump", "lively"));
            });
        boots.build(store, 50);


        ItemStoreBuilder swords = items.buildItemStore(ItemType.SWORDS.getMaterials())
            .withAmount(1)
            .withDurabilityPercentages(10, 20, 30, 40, 50, 60, 70, 80, 90, 100)
            .withEnchantmentNumbers(1, 2, 3)
            .withEnchantments(Enchantment.DAMAGE_ALL, 1, 2, 3, 4, 5)
            .withEnchantments(Enchantment.KNOCKBACK, 1, 2)
            .withEnchantments(Enchantment.FIRE_ASPECT, 1, 2)
            .withEnchantments(Enchantment.DAMAGE_UNDEAD, 1, 2, 3, 4, 5)
            .withEnchantments(Enchantment.DURABILITY, 1, 2, 3)
            .withModifier((isb, item) -> {
                addCustomEnchant(item, Lists.newArrayList("fervor", "knockup", "wither"));
            });
        swords.build(store, 50);


        ItemStoreBuilder bows = items.buildItemStore(ItemType.BOWS.getMaterials())
            .withAmount(1)
            .withDurabilityPercentages(10, 20, 30, 40, 50, 60, 70, 80, 90, 100)
            .withEnchantmentNumbers(1, 2, 3)
            .withEnchantments(Enchantment.ARROW_DAMAGE, 1, 2, 3, 4, 5)
            .withEnchantments(Enchantment.ARROW_KNOCKBACK, 1, 2)
            .withEnchantments(Enchantment.ARROW_FIRE, 1)
            .withEnchantments(Enchantment.ARROW_INFINITE, 1)
            .withEnchantments(Enchantment.DURABILITY, 1, 2, 3);
        bows.build(store, 50);


        ItemStoreBuilder tools = items.buildItemStore(ItemType.TOOLS.getMaterials())
            .withAmount(1)
            .withDurabilityPercentages(10, 20, 30, 40, 50, 60, 70, 80, 90, 100)
            .withEnchantmentNumbers(1, 2, 3)
            .withEnchantments(Enchantment.DIG_SPEED, 2, 3, 4, 5)
            .withEnchantments(Enchantment.LOOT_BONUS_BLOCKS, 1, 2, 3)
            .withEnchantments(Enchantment.DURABILITY, 2, 3)
            .withModifier((isb, item) -> {
                addCustomEnchant(item, Lists.newArrayList("blind", "slow", "berserking", "lightning", "lifesteal", "poison", "weakness"));
            });
        tools.build(store, 50);


        Lists.newArrayList(
            PotionEffectType.REGENERATION
        );


        ItemStoreBuilder potions = items.buildItemStore(ItemType.POTIONS.getMaterials())
            .withAmount(1)
            .withModifier((isb, item) -> {

                List<PotionType> possibleTypes = new ArrayList<>();
                switch (item.getType()) {
                    case POTION:
                        possibleTypes.addAll(PotionTypes.POSITIVE_EFFECTS.getEffectTypes());
                        break;
                    case SPLASH_POTION:
                    case LINGERING_POTION:
                        possibleTypes.addAll(PotionTypes.POSITIVE_EFFECTS.getEffectTypes());
                        possibleTypes.addAll(PotionTypes.NEGATIVE_EFFECTS.getEffectTypes());
                        break;
                    default:
                        break;
                }

                if (possibleTypes.isEmpty()) {
                    return;
                }

                PotionType potionType = Randoms.randomElement(possibleTypes);
                PotionMeta potionMeta = (PotionMeta) item.getItemMeta();
                boolean extended = potionType.isExtendable() && Randoms.randomBoolean();
                boolean upgraded = potionType.isUpgradeable() && !extended && Randoms.randomBoolean();
                PotionData potionData = new PotionData(potionType, extended, upgraded);
                potionMeta.setBasePotionData(potionData);

                if (Randoms.randomInteger(1, 100) <= Constants.CUSTOM_POTION_CHANCE) {

                    // when adding custom effect, reset base potion as not extended nor upgraded
                    potionData = new PotionData(potionType, false, false);
                    potionMeta.setBasePotionData(potionData);

                    if (PotionTypes.isPositiveEffect(potionType)) {
                        if (Randoms.randomBoolean()) {
                            potionMeta.addCustomEffect(new PotionEffect(PotionEffectType.SPEED, 800, 9), true);
                        } else {
                            potionMeta.addCustomEffect(new PotionEffect(PotionEffectType.JUMP, 100, 49), true);
                            potionMeta.addCustomEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 250, 99), true);
                        }
                    } else if (PotionTypes.isNegativeEffect(potionType)) {
                        int randomInt = Randoms.randomInteger(1, 6);
                        switch (randomInt) {
                            case 1:
                                potionMeta.addCustomEffect(new PotionEffect(PotionEffectType.POISON, 300, 6), true);
                                break;
                            case 2:
                                potionMeta.addCustomEffect(new PotionEffect(PotionEffectType.HARM, 0, 8), true);
                                break;
                            case 3:
                                potionMeta.addCustomEffect(new PotionEffect(PotionEffectType.WEAKNESS, 300, 8), true);
                                break;
                            case 4:
                                potionMeta.addCustomEffect(new PotionEffect(PotionEffectType.BLINDNESS, 300, 8), true);
                                break;
                            case 5:
                                potionMeta.addCustomEffect(new PotionEffect(PotionEffectType.CONFUSION, 300, 6), true);
                                break;
                            case 6:
                                potionMeta.addCustomEffect(new PotionEffect(PotionEffectType.LEVITATION, 300, 6), true);
                                break;
                        }
                    }

                }

                item.setItemMeta(potionMeta);
            });
        potions.build(store, 150);

        ItemStoreBuilder food = items.buildItemStore(ItemType.FOOD.getMaterials())
            .withAmounts(1, 2, 3)
            .withDurabilities(0, 1);
        food.build(store, 50);


        ItemStoreBuilder other = items.buildItemStore(ItemType.OTHER.getMaterials())
            .withAmountsRange(1, 16);
        other.build(store, 50);

        items.addItemStore(Constants.ALL_ITEM_STORE, store);

        Log.info("Successfuly loaded items store in " + (System.currentTimeMillis() - start) + "ms");


        // load locations
        List<String> strLoc = api.getConfig().getStringList("random-filled-chests.locations");
        if (strLoc != null) {
            World world = api.getWorldConfig().getWorld();
            for (String str : strLoc) {
                locations.add(Parser.parseLocation(world, str));
            }
        }

        minItems = api.getConfig().getInt("random-filled-chests.min", Constants.MIN_CHEST_ITEMS);
        maxItems = api.getConfig().getInt("random-filled-chests.max", Constants.MAX_CHEST_ITEMS);

    }

    private void addCustomEnchant(ItemStack itemStack, List<String> enchantNames) {
        if (Randoms.randomInteger(1, 100) <= Constants.CUSTOM_ENCHANT_CHANCE) {

            String enchantName = enchantNames.get(Randoms.randomInteger(0, enchantNames.size() - 1));

            EnchantmentAPI.getEnchantment(enchantName).addToItem(itemStack, Randoms.randomInteger(1, 3));

            // add custom enchant explanation lore
            ItemMeta meta = itemStack.getItemMeta();
            List<String> lore = meta.getLore();
            if (lore == null) {
                lore = new ArrayList<>();
            }
            lore.add(" ");
            lore.addAll(Lists.newArrayList(
                api.getStringsAPI()
                    .get("lostworld.items.custom-enchantments." + enchantName)
                    .toString()
                    .split(Pattern.quote(","))
                )
            );
            meta.setLore(lore);
            itemStack.setItemMeta(meta);

            if (itemStack.getEnchantments().isEmpty()) {
                FakeGlowEnchant.addGlow(itemStack);
            }

        }

    }

}
