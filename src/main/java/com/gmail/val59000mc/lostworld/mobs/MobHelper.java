package com.gmail.val59000mc.lostworld.mobs;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.lostworld.common.Constants;
import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;

public class MobHelper {

	private HCGameAPI api;

	public MobHelper(HCGameAPI api) {
		this.api = api;
	}

	/**
	 * Handle bob killed
	 * @param killer
	 */
	public void handleBobKilled(Player killer) {
		
		if(killer == null){
			api.getStringsAPI().get("lostworld.mobs.bob-death").sendChatP();
		}else{
			LostWorldPlayer lwPlayer = (LostWorldPlayer) api.getPlayersManagerAPI().getHCPlayer(killer);
			api.getStringsAPI()
				.get("lostworld.mobs.bob-death-player")
				.replace("%player%", lwPlayer.getColoredName())
				.sendChatP();
			
			double bobKillReward = api.getConfig().getDouble("objectives.BOB.rewards.on-bob-kill", Constants.BOB_KILL_REWARD);
			api.getPlayersManagerAPI().rewardMoneyTo(lwPlayer, bobKillReward);
		}
		
		api.getStringsAPI().get("lostworld.mobs.bob-death-title")
			.sendTitle(20, 60, 20);
		
	}
	
}
