package com.gmail.val59000mc.lostworld.collects;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.lostworld.callbacks.LostWorldCallbacks;
import com.gmail.val59000mc.lostworld.events.BobCollectedEvent;
import com.gmail.val59000mc.lostworld.items.LostWorldtems;
import com.gmail.val59000mc.lostworld.objectives.ObjectiveType;
import com.gmail.val59000mc.lostworld.objectives.impl.BobObjective;
import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;
import com.gmail.val59000mc.lostworld.players.LostWorldTeam;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class CollectBobHeadAction extends CollectAction{

	private BobObjective objective;

	public CollectBobHeadAction(HCGameAPI api, LostWorldTeam team) {
		super(api, team);
		this.objective = (BobObjective) ((LostWorldCallbacks) getApi().getCallbacksApi()).getObjectiveManager().getObjective(ObjectiveType.BOB);
	}

	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {
		LostWorldPlayer lwPlayer = checkPlayerTeam(player);
		if(lwPlayer == null){
			executeNextAction(player, sigPlayer, false);
			return;
		}

		if(objective.isCompleted()){
			getApi().getStringsAPI().get("lostworld.objectives.already-completed")
				.replace("%objective%", getApi().getStringsAPI().getNoColor("lostworld.objectives."+objective.getType().name()+".name").toString())
				.sendChat(lwPlayer);
			executeNextAction(player, sigPlayer, false);
			return;
		}
		
		boolean hasHead = LostWorldtems.hasBobHead(player);
		
		if(hasHead){
			LostWorldtems.removeBobHead(player);
			getApi().callEvent(new BobCollectedEvent(getApi(), lwPlayer));
			executeNextAction(player, sigPlayer, true);
		}else{
			getApi().getStringsAPI().get("lostworld.objectives.BOB.no-head").sendChat(lwPlayer);
			executeNextAction(player, sigPlayer, false);
		}
	}

}
