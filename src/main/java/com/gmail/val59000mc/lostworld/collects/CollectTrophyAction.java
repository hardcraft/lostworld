package com.gmail.val59000mc.lostworld.collects;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.lostworld.callbacks.LostWorldCallbacks;
import com.gmail.val59000mc.lostworld.events.TrophyCollectedEvent;
import com.gmail.val59000mc.lostworld.items.LostWorldtems;
import com.gmail.val59000mc.lostworld.objectives.ObjectiveType;
import com.gmail.val59000mc.lostworld.objectives.impl.TrophiesObjective;
import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;
import com.gmail.val59000mc.lostworld.players.LostWorldTeam;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;

public class CollectTrophyAction extends CollectAction{

	private TrophiesObjective objective;
	
	public CollectTrophyAction(HCGameAPI api, LostWorldTeam team) {
		super(api, team);
		this.objective = (TrophiesObjective) ((LostWorldCallbacks) getApi().getCallbacksApi()).getObjectiveManager().getObjective(ObjectiveType.TROPHIES);
	}
	
	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {
		LostWorldPlayer lwPlayer = checkPlayerTeam(player);
		if(lwPlayer == null){
			executeNextAction(player, sigPlayer, false);
			return;
		}
		
		if(objective.isCompleted()){
			getApi().getStringsAPI().get("lostworld.objectives.already-completed")
				.replace("%objective%", getApi().getStringsAPI().getNoColor("lostworld.objectives."+objective.getType().name()+".name").toString())
				.sendChat(lwPlayer);
			executeNextAction(player, sigPlayer, false);
			return;
		}
		
		int count = LostWorldtems.countTrophyItems(player);
		
		if(count > 0){
			LostWorldtems.removeTrophyItems(player, count);
			getApi().callEvent(new TrophyCollectedEvent(getApi(), lwPlayer, count));
			executeNextAction(player, sigPlayer, true);
		}else{
			getApi().getStringsAPI().get("lostworld.objectives.TROPHIES.no-trophy").sendChat(lwPlayer);
			executeNextAction(player, sigPlayer, false);
		}
	}

}
