package com.gmail.val59000mc.lostworld.collects;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.lostworld.players.LostWorldPlayer;
import com.gmail.val59000mc.lostworld.players.LostWorldTeam;
import com.gmail.val59000mc.simpleinventorygui.actions.Action;

public abstract class CollectAction extends Action{

	private HCGameAPI api;
	private LostWorldTeam team;
	
	public CollectAction(HCGameAPI api, LostWorldTeam team) {
		this.api = api;
		this.team = team;
	}

	protected HCGameAPI getApi() {
		return api;
	}

	protected LostWorldTeam getTeam() {
		return team;
	}
	
	protected LostWorldPlayer checkPlayerTeam(Player player){
		LostWorldPlayer lwPlayer = (LostWorldPlayer) api.getPlayersManagerAPI().getHCPlayer(player);
		if(lwPlayer != null && team.equals(lwPlayer.getTeam()) && lwPlayer.isPlaying()){
			return lwPlayer;
		}
		return null;
	}
	
	
}
